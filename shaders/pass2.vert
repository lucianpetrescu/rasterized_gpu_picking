#version 430

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

layout (location = 0) in vec3 in_position;
layout (location = 1) in float in_ttl;
layout (location = 2) in vec3 in_startposition;
layout (location = 3) in vec3 in_speed; 
layout (location = 4) in vec3 in_other;		//oldttl size type

out vec4 other;

out ivec2 v_g_ids;

void main()
{    
    other = vec4(in_ttl,in_other);
    mat4 scaleMatrix = mat4(vec4(10,0,0,0),vec4(0,10,0,0),vec4(0,0,10,0),vec4(0,0,0,1));
    v_g_ids = ivec2(gl_VertexID, gl_InstanceID);
    
    vec3 pos = in_position;
    vec4 newpos = scaleMatrix*vec4(pos,1);
    newpos.x+=3500;
    newpos.z+=3500;
    gl_Position = viewMatrix * modelMatrix * newpos;
    //gl_Position = viewMatrix * modelMatrix * scaleMatrix*vec4(pos,1);
}