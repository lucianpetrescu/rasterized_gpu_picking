#version 430

uniform sampler2D alphaMap;
uniform sampler2D aoMap;
uniform sampler2D detailMap;
uniform sampler2D diffuseMap;
uniform sampler2D displacementMap;
uniform sampler2D dudvMap;
uniform sampler2D emissiveMap;
uniform sampler2D extraMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap; 


struct LightStructType{
	vec3 position;
	vec3 color;
};
struct MaterialStructType{		//in aceasta ordine
	float hastextures[10];		//alpha,ao,detail, diffuse, displacement, dudv, emissive, extra, nmap, specular;
	vec3 Kad;
	vec3 Ke;
	vec3 Ks;
	float shininess;
};
uniform LightStructType Light;
uniform MaterialStructType Material;
uniform int alpha_culling_enable;

//from vertex shader
in vec3 position;
in vec3 normal;
in vec3 tangent;
in vec2 texcoord;
in vec3 barycentric;

layout (location =0) out vec4 outputFramebuffer;

//constant
vec3 ambientLight=vec3(0.1,0.1,0.1);
vec2 texcoord_dx, texcoord_dy;


//PICKING
flat in ivec2 tes_f_ids;
flat in int tes_f_tess_id;

//picking
uniform int picking_id;
uniform float picking_now;
uniform float picking_show;
uniform vec2 picking_xy;
layout(binding = 4,offset=0) uniform atomic_uint picking_atomicbuffer;
struct PickingFragment
{
	int objectId;
	int primitiveId;
	int vertexId;
	int instanceId;
	float invocationId;
	float lightPercent;
	float alpha;
	float depth;
};
layout(std140, binding = 3) buffer PickingFragmentBuffer
{
	PickingFragment picking_fragments[];
};


void main()
{	
	vec3 N = normalize(normal);
	vec3 T = normalize(tangent);
	vec3 B = normalize(cross(T,N));
	N = mat3(T,B,N)*( texture(normalMap,texcoord).xyz*2-1);
	vec3 L = normalize( Light.position - position);
	vec3 diffuseLight = Light.color*max(dot(L,N),0);
	float specpower = 1;
	if(Material.hastextures[9]>0.5) specpower = texture(specularMap, texcoord).x;
	vec3 albeldo = Material.Kad;
	if(Material.hastextures[3]>0.5) albeldo=texture(diffuseMap, texcoord).xyz;
	
	float alpha=1;
	if(Material.hastextures[1]>0.5 && alpha_culling_enable == 1){
		alpha = texture(alphaMap, texcoord).x;
		if(alpha<0.3) discard;
		else alpha=1;
	}
	vec3 color= ambientLight+diffuseLight*albeldo;
	float illumination = diffuseLight.x;
		
	
	if(picking_now>0.5){
		if( int(gl_FragCoord.x) == int(picking_xy.x) && int(gl_FragCoord.y) == int(picking_xy.y) ){
			uint count = atomicCounterIncrement(picking_atomicbuffer);
			if(count==0){
				picking_fragments[0].objectId    = picking_id;
				picking_fragments[0].primitiveId = gl_PrimitiveID;
				picking_fragments[0].vertexId    = tes_f_ids.x;		//vertexID
				picking_fragments[0].instanceId  = tes_f_ids.y;		//instanceID
				picking_fragments[0].invocationId= tes_f_tess_id;	//invocationID
				picking_fragments[0].lightPercent= illumination;
				picking_fragments[0].alpha       = alpha;
				picking_fragments[0].depth       = pow(gl_FragCoord.z,1024);;
			}else{
				float previousdepth = picking_fragments[0].depth;
				if( previousdepth> pow(gl_FragCoord.z,1024)){
					picking_fragments[0].objectId    = picking_id;
					picking_fragments[0].primitiveId = gl_PrimitiveID;
					picking_fragments[0].vertexId    = tes_f_ids.x;
					picking_fragments[0].instanceId  = tes_f_ids.y;
					picking_fragments[0].invocationId= tes_f_tess_id;;
					picking_fragments[0].lightPercent= illumination;
					picking_fragments[0].alpha       = alpha;
					picking_fragments[0].depth       = pow(gl_FragCoord.z,1024);;
				}
			}
			
			picking_fragments[count+2].objectId    = picking_id;
			picking_fragments[count+2].primitiveId = gl_PrimitiveID;
			picking_fragments[count+2].vertexId    = tes_f_ids.x;
			picking_fragments[count+2].instanceId  = tes_f_ids.y;
			picking_fragments[count+2].invocationId= tes_f_tess_id;;
			picking_fragments[count+2].lightPercent= illumination;
			picking_fragments[count+2].alpha       = alpha;
			picking_fragments[count+2].depth       = pow(gl_FragCoord.z,1024);
			
			memoryBarrier();
			
		}
	}
	
	if(picking_show>0.5 && picking_now<0.5){
		if( picking_fragments[0].objectId == picking_id && picking_fragments[0].instanceId  == tes_f_ids.y){
				color = vec3(1,0,0);
				if(picking_fragments[0].primitiveId == gl_PrimitiveID){
					color=vec3(0,1,0);
					if(picking_fragments[0].vertexId == tes_f_ids.x && picking_fragments[0].invocationId == tes_f_tess_id){
						color = vec3(0,0,1);
					}
				}
			}
	}
	
	
	outputFramebuffer = vec4(color,1);
}

