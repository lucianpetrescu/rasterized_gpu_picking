#version 430
layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;  

uniform vec2 screen;
out vec2 texcoord;

void main()
{        
	//screen space -> ndc space
	vec2 ndcpos = (in_position - screen/2.0f)/(screen/2);

	texcoord = in_texcoord;
    gl_Position = vec4(ndcpos,0,1);
}