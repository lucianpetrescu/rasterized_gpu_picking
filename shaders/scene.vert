#version 430

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_normal; 
layout (location = 2) in vec4 in_tangent;
layout (location = 3) in vec2 in_texcoord;  

out vec3 v_tcs_normal;
out vec3 v_tcs_tangent;
out vec2 v_tcs_texcoord;

uniform float isinstanced;
uniform samplerBuffer dataBuffer;
uniform float time;

out ivec2 v_tcs_ids;

void main(){

	v_tcs_normal = in_normal;
	v_tcs_tangent = in_tangent.xyz;
	v_tcs_texcoord = in_texcoord;
	
	v_tcs_ids.x = gl_VertexID;
	v_tcs_ids.y = gl_InstanceID;
	
	vec3 position = vec3(0,0,0);
	if(isinstanced <0.5){
		position = in_position;
	}else{
		vec4 data = texelFetch(dataBuffer, gl_InstanceID);
		//float angle = data.z + 0.4*cos(time*0.0001 + (gl_InstanceID+1)/26.0f);
		float angle = data.z + 0.4*cos(0.0001 + (gl_InstanceID+1)/26.0f);
		float cosu = cos(angle);
		float sinu = sin(angle);
		mat3 rotmatrix = mat3(vec3(cosu,0,-sinu),vec3(0,1,0),vec3(sinu,0,cosu));
		vec3 newposition =rotmatrix*in_position;
		position = vec3(data.x+newposition.x*data.w, newposition.y*data.w, data.y+newposition.z*data.w);
	}
	gl_Position = vec4(position,1);
}