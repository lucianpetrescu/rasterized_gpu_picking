#version 430

uniform mat4 projMatrix;

layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

in vec4 other[];
out vec2 texcoord;
out vec4 fother;

in ivec2 v_g_ids[];
flat out ivec3 g_f_ids;

void main()
{    
	float d=other[0].z/2.0*5*20;
	
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(-d,-d,0,0) );	
	texcoord = vec2(0,0);	fother = other[0]; 
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x);
	
	EmitVertex();
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(d,-d,0,0) );
	texcoord = vec2(1,0);	fother = other[0];
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x);
	
	EmitVertex();
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(d,d,0,0) );
	texcoord = vec2(1,1);	fother = other[0];
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x);
	EmitVertex();
	EndPrimitive();
	
	
	
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(-d,-d,0,0) );	
	texcoord = vec2(0,0);	fother = other[0];
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x+1);
	EmitVertex();
	
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(d,d,0,0) );
	texcoord = vec2(1,1);	fother = other[0];
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x+1);
	EmitVertex();	
	
	gl_Position = projMatrix* (gl_in[0].gl_Position + vec4(-d,d,0,0) );
	texcoord = vec2(0,1);	fother = other[0];
	g_f_ids = ivec3(v_g_ids[0],2*v_g_ids[0].x+1);
	EmitVertex();
	EndPrimitive();			
}