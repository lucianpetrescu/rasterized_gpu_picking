#version 430

in vec2 texcoord;
uniform sampler2D uiTexture;
 
layout (location =0) out vec4 outputFramebuffer;

void main()
{
	vec3 data = texture2D(uiTexture,texcoord).xyz;
	if(data.x<0.4) discard;
	outputFramebuffer=vec4(0,0,0,1);
}