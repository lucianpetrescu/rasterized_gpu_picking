#version 430
uniform sampler2D texMap1;
uniform sampler2D texMap2;

layout (location =0) out vec4 outputFramebuffer;

in vec2 texcoord;
in vec4 fother;		//ttl, oldttl, size, type

uniform float ttlfade;


//PICKING
flat in ivec3 g_f_ids;
uniform int picking_id;
uniform float picking_now;
uniform float picking_show;
uniform vec2 picking_xy;

layout(binding = 4,offset=0) uniform atomic_uint picking_atomicbufferAll;
layout(binding = 4,offset=4) uniform atomic_uint picking_atomicbufferTransparent;
struct PickingFragmentData
{
	int objectId;
	int primitiveId;
	int vertexId;
	int instanceId;
	float invocationId;
	float lightPercent;
	float alpha;
	float depth;
};
layout(std140, binding = 3) buffer PickingFragmentBuffer
{
	PickingFragmentData picking_fragments[];
};


void main()
{
	
	vec3 color;
	if(fother.w>0) color=texture(texMap1, texcoord).xyz;
	else color = texture(texMap2, texcoord).xyz;
	
	float alpha=color.x;
	float dist = distance(texcoord, vec2(0.5,0.5));
	alpha *= (1 - pow(dist,0.22));
	alpha *= fother.x / ttlfade;


	if(picking_now>0.5){
		if( (abs(gl_FragCoord.x-picking_xy.x)<0.51) && (abs(gl_FragCoord.y-picking_xy.y)<0.51) ){
			uint countTrans = atomicCounterIncrement(picking_atomicbufferTransparent);
			uint countAll= atomicCounterIncrement(picking_atomicbufferAll);
			if(countTrans==0){
				picking_fragments[1].objectId    = picking_id;
				picking_fragments[1].primitiveId = g_f_ids.z;
				picking_fragments[1].vertexId    = g_f_ids.x;
				picking_fragments[1].instanceId  = g_f_ids.y;
				picking_fragments[1].invocationId= 0;
				picking_fragments[1].lightPercent= 0;
				picking_fragments[1].alpha       = alpha;
				picking_fragments[1].depth       = pow(gl_FragCoord.z,1024);
			}else{
				if(picking_fragments[1].depth > pow(gl_FragCoord.z,1024)){
					picking_fragments[1].objectId    = picking_id;
					picking_fragments[1].primitiveId = g_f_ids.z;
					picking_fragments[1].vertexId    = g_f_ids.x;
					picking_fragments[1].instanceId  = g_f_ids.y;
					picking_fragments[1].invocationId= 0;
					picking_fragments[1].lightPercent= 0;
					picking_fragments[1].alpha       = alpha;
					picking_fragments[1].depth       = pow(gl_FragCoord.z,1024);
				}
			}
			
			memoryBarrier();
		}
	}
	
	if(picking_show>0.5 && picking_now<0.5){
		if( picking_fragments[1].objectId == picking_id){
			color = vec3(1,0,0);
				if(picking_fragments[1].vertexId  == g_f_ids.x){
					color = vec3(0,1,0);
					alpha+=0.25;
					if(picking_fragments[1].primitiveId ==g_f_ids.z){
						color=vec3(0,0,1);
					}
				}
		}
	}
	
	
	outputFramebuffer = vec4(color, alpha);
}