//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#include "camera.h"


namespace rgp {
	//simple FPS camera
	Camera::Camera() {
		position = glm::vec3(0, 0, 50);
		forward = glm::vec3(0, 0, -1);
		up = glm::vec3(0, 1, 0);
		right = glm::vec3(1, 0, 0);
		computeView();
	}
	Camera::Camera(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up) {
		setView(position, focus, up);
	}
	Camera::~Camera() {
	}

	void Camera::setView(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up) {
		this->position = position;
		forward = glm::normalize(focus - position);
		right = glm::cross(forward, up);
		this->up = glm::cross(right, forward);
		computeView();
	}

	void Camera::translateForward(float distance) {
		position = position + glm::normalize(glm::vec3(forward.x, 0, forward.z))*distance;
		computeView();
	}
	void Camera::translateUpword(float distance) {
		position = position + glm::normalize(glm::vec3(0, up.y, 0))*distance;
		computeView();
	}
	void Camera::translateRight(float distance) {
		position = position + glm::normalize(glm::vec3(right.x, 0, right.z))*distance;
		computeView();
	}

	void Camera::rotateX(float angle) {
		if ((forward.y > 0.9 && angle > 0) || (forward.y < -0.9 && angle < 0)) return;
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, right)*glm::vec4(forward, 1))));
		computeView();
	}
	void Camera::rotateY(float angle) {
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(forward, 1))));
		right = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(right, 1))));
		computeView();
	}

	const glm::mat4& Camera::getViewMatrix() {
		return view_matrix;
	}

	const glm::vec3& Camera::getPosition() {
		return position;
	}
	const glm::vec3& Camera::getUp() {
		return up;
	}
	const glm::vec3& Camera::getForward() {
		return forward;
	}
	const glm::vec3 Camera::getFocus() {
		return position + forward;
	}
	const glm::vec3& Camera::getRight() {
		return right;
	}

	void Camera::computeView() {
		view_matrix = glm::lookAt(position, position + glm::normalize(forward), up);
	}
}