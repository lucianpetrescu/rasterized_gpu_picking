#pragma once
#include "dependencies.h"

namespace rgp {
	struct PickingFragment {
	public:
		int objectId;
		int primitiveId;
		int vertexId;
		int instanceId;
		float invocationId;
		float lightPercent;
		float alpha;
		float depth;
	};

	class PickingBuffer {
	public:
		PickingBuffer();
		~PickingBuffer();
		void init(int count);
		void bindForReadWrite(unsigned int unitPickBuffer, unsigned int unitAtomicBuffer);
		void getData();
		int getFragmentCountSolid();
		int getFragmentCountTransparent();
		int getMemoryUsed();

	public:
		//shader storage buffer for data
		unsigned int pickBuffer =0;
		int pickBufferSize =0;
		unsigned int atomicbuffer =0;
		int fragment_count_solid =0;
		int fragment_count_transparent =0;
		int memory_used =0;
		PickingFragment fragments[1000];
	};
}