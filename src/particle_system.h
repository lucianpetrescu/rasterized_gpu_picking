#pragma once
#include "shader.h"
#include <sstream>

namespace rgp {

	class ParticlePerturbator {
	public:
		ParticlePerturbator(float px, float py, float pz, float range, float power, float vx, float vy, float vz);
		float px, py, pz, range;	//packed in vec4	
		float vx, vy, vz, power;	//packed in vec4
	};

	class ParticleDynamic {
	public:
		float px, py, pz;		// current x y z
		float ttl;
	};
	class ParticleStatic {
	public:
		float sx, sy, sz;		// start x y z	
		float vx, vy, vz;		// speed .. no acceleration in this model
		float oldttl;			// old ttl
		float size;				// particle size
		float type;
	};

	class ParticleSystem {
	public:
		ParticleSystem();
		~ParticleSystem();

		void create(int count, float timetolive, float timetostart, float timetolivefade, float size);
		void reloadShaders();
		void computeCPU();
		void computeGPU();
		void simulate(bool withgpu);
		void render(const glm::mat4& ModelMatrix, const glm::mat4& ViewMatrix, const glm::mat4& ProjMatrix);
		void addPerturbator(const ParticlePerturbator& a);
		void clearPerturbators();
	private:
		void littletokenizer(const std::string& str, std::vector<std::string> *toks);
		void mangleShader();
	public:
		Shader& getProgramRender();

	public:
		//program shadere
		Shader shaderPass1, shaderPass2;

		//buffere
		unsigned int vao[2] = { 0,0 };
		unsigned int vbostatic = 0, vbo[2] = { 0,0 };
		bool ping = true;

		//ubo
		unsigned int ubo_buffer =0;
		unsigned int ubo_binding_point =0;
		unsigned int ubo_block_index =0;

		//textura
		unsigned int idtextura1 =0;
		unsigned int idtextura2 =0;
		unsigned int idsampler =0;

		//date efective
		float timetolivefade =0;
		float timetostart =0;
		float timetolive =0;
		std::vector<ParticleDynamic> particlesDynamic;
		std::vector<ParticleStatic> particlesStatic;
		std::vector<ParticlePerturbator> perturbators;
	};
}