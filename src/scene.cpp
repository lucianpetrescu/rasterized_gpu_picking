#include "scene.h"

namespace rgp {
	//material  -----------------------------------------------------------------------------------
	SceneMaterial::SceneMaterial() {
		name = "";
		shininess = alpha = kad[0] = kad[1] = kad[2] = ks[0] = ks[1] = ks[2] = ke[0] = ke[1] = ke[2] = 0;
		has_aomap = has_alphamap = has_detailmap = has_diffusemap = has_displacementmap = has_dudvmap = has_emissivemap = has_extramap = has_normalmap = has_specularmap = false;
		aomap = alphamap = detailmap = diffusemap = displacementmap = dudvmap = emissivemap = extramap = normalmap = specularmap = 0;
		programlast = -1;
		for (int i = 0; i < 15; i++)loc[i] = -1;
	}
	void SceneMaterial::generateTexture(unsigned int *map, int map_x, int map_y, unsigned char *data, bool threebytes) {
		unsigned int tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		if (threebytes) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, map_x, map_y, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		else glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, map_x, map_y, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		(*map) = tex;
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	void SceneMaterial::init() {
		if (has_aomap) { generateTexture(&aomap, aomap_x, aomap_y, aoData, false);														free(aoData); }
		if (has_alphamap) { generateTexture(&alphamap, alphamap_x, alphamap_y, alphaData, false);										free(alphaData); }
		if (has_detailmap) { generateTexture(&detailmap, detailmap_x, detailmap_y, detailData, true);									free(detailData); }
		if (has_diffusemap) { generateTexture(&diffusemap, diffusemap_x, diffusemap_y, diffuseData, true);								free(diffuseData); }
		if (has_displacementmap) { generateTexture(&displacementmap, displacementmap_x, displacementmap_y, displacementData, false);	free(displacementData); }
		if (has_dudvmap) { generateTexture(&dudvmap, dudvmap_x, dudvmap_y, dudvData, true);												free(dudvData); }
		if (has_emissivemap) { generateTexture(&emissivemap, emissivemap_x, emissivemap_y, emissiveData, true);							free(emissiveData); }
		if (has_extramap) { generateTexture(&extramap, extramap_x, extramap_y, extraData, true);										free(extraData); }
		if (has_normalmap) { generateTexture(&normalmap, normalmap_x, normalmap_y, normalData, true);									free(normalData); }
		if (has_specularmap) { generateTexture(&specularmap, specularmap_x, specularmap_y, specularData, false);						free(specularData); }
	}

	void SceneMaterial::bindtexture(int target, unsigned int map, int loc) {
		glActiveTexture(GL_TEXTURE0 + target);
		glBindTexture(GL_TEXTURE_2D, map);
		glUniform1i(loc, target);
	}
	void SceneMaterial::use(Shader& shader) {
		int program = shader.GetGLShader();
		if (program != programlast) {
			programlast = program;
			loc[0] = glGetUniformLocation(program, "aoMap");
			loc[1] = glGetUniformLocation(program, "alphaMap");
			loc[2] = glGetUniformLocation(program, "detailMap");
			loc[3] = glGetUniformLocation(program, "diffuseMap");
			loc[4] = glGetUniformLocation(program, "displacementMap");
			loc[5] = glGetUniformLocation(program, "dudvMap");
			loc[6] = glGetUniformLocation(program, "emissiveMap");
			loc[7] = glGetUniformLocation(program, "extraMap");
			loc[8] = glGetUniformLocation(program, "normalMap");
			loc[9] = glGetUniformLocation(program, "specularMap");
			loc[10] = glGetUniformLocation(program, "Material.hastextures");
			loc[11] = glGetUniformLocation(program, "Material.Kad");
			loc[12] = glGetUniformLocation(program, "Material.Ke");
			loc[13] = glGetUniformLocation(program, "Material.Ks");
			loc[14] = glGetUniformLocation(program, "Material.shininess");
		}


		int target = 0;
		std::vector<float> hastexture;
		if (has_aomap) { bindtexture(target, aomap, loc[0]);						target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_alphamap) { bindtexture(target, alphamap, loc[1]);				target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_detailmap) { bindtexture(target, detailmap, loc[2]);			target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_diffusemap) { bindtexture(target, diffusemap, loc[3]);		target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_displacementmap) { bindtexture(target, displacementmap, loc[4]); target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_dudvmap) { bindtexture(target, dudvmap, loc[5]);				target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_emissivemap) { bindtexture(target, emissivemap, loc[6]);	target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_extramap) { bindtexture(target, extramap, loc[7]);				target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_normalmap) { bindtexture(target, normalmap, loc[8]);			target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);
		if (has_specularmap) { bindtexture(target, specularmap, loc[9]);	target++;	hastexture.push_back(1); }
		else hastexture.push_back(0);

		//int loc;
		glUniform1fv(loc[10], 10, &hastexture[0]);
		glUniform3f(loc[11], kad[0], kad[1], kad[2]);
		glUniform3f(loc[12], ke[0], ke[1], ke[2]);
		glUniform3f(loc[13], ks[0], ks[1], ks[2]);
		glUniform1f(loc[14], shininess);

	}
	SceneMaterial::~SceneMaterial() {
		if (has_alphamap)			glDeleteTextures(1, &alphamap);
		if (has_aomap)				glDeleteTextures(1, &aomap);
		if (has_detailmap)			glDeleteTextures(1, &detailmap);
		if (has_diffusemap)			glDeleteTextures(1, &diffusemap);
		if (has_displacementmap)	glDeleteTextures(1, &displacementmap);
		if (has_dudvmap)			glDeleteTextures(1, &dudvmap);
		if (has_emissivemap)		glDeleteTextures(1, &emissivemap);
		if (has_extramap)			glDeleteTextures(1, &extramap);
		if (has_normalmap)			glDeleteTextures(1, &normalmap);
		if (has_specularmap)		glDeleteTextures(1, &specularmap);
	}
	
	//obiect de scena  -----------------------------------------------------------------------------------
	SceneObject::SceneObject() {
	};
	void SceneObject::init() {
		this->name = name;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &verticesvbo);
		glBindBuffer(GL_ARRAY_BUFFER, verticesvbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) * 3, &vertices[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);

		glGenBuffers(1, &normalsvbo);
		glBindBuffer(GL_ARRAY_BUFFER, normalsvbo);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float) * 3, &normals[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, 0, 0, 0);

		glGenBuffers(1, &tangentsvbo);
		glBindBuffer(GL_ARRAY_BUFFER, tangentsvbo);
		glBufferData(GL_ARRAY_BUFFER, tangents.size() * sizeof(float) * 4, &tangents[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, 0, 0, 0);

		glGenBuffers(1, &texcoordsvbo);
		glBindBuffer(GL_ARRAY_BUFFER, texcoordsvbo);
		glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float) * 2, &texcoords[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 2, GL_FLOAT, 0, 0, 0);

		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

		indicestodraw = (int)indices.size();
		vertices.clear();
		normals.clear();
		tangents.clear();
		texcoords.clear();
		indices.clear();
	}
	void SceneObject::draw() {
		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indicestodraw, GL_UNSIGNED_INT, (void*)0);
	}
	void SceneObject::drawPatches() {
		glBindVertexArray(vao);
		glPatchParameteri(GL_PATCH_VERTICES, 3);	//3 verts form a patch
		glDrawElements(GL_PATCHES, indicestodraw, GL_UNSIGNED_INT, (void*)0);
	}
	SceneObject::~SceneObject() {
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &verticesvbo);
		glDeleteBuffers(1, &normalsvbo);
		glDeleteBuffers(1, &tangentsvbo);
		glDeleteBuffers(1, &texcoordsvbo);
		glDeleteBuffers(1, &ibo);
	}
	
	// scene loader  -----------------------------------------------------------------------------------
	Scene::Scene() {
		totalMaterials = totalObjects = totalTextures = totalTextureSize = totalTriangles = totalVertices = 0; 
	}
	Scene::~Scene() {
		for (unsigned int i = 0; i < sceneObjects.size(); i++) delete sceneObjects[i];
		for (unsigned int i = 0; i < sceneMaterials.size(); i++) delete sceneMaterials[i];
	}

	bool Scene::load(std::string scenePakFile) {
		for (unsigned int i = 0; i < sceneObjects.size(); i++) delete sceneObjects[i];
		for (unsigned int i = 0; i < sceneMaterials.size(); i++) delete sceneMaterials[i];
		sceneObjects.clear();
		sceneMaterials.clear();

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		int oldmaterialcount = (int)sceneMaterials.size();

		double clockstart = std::clock();
		//incarca material
		FILE *file;
		fopen_s(&file, scenePakFile.c_str(), "rb");
		if (!file) {
			std::cout << "ERROR: wrong filename and/or path. Closing..." << std::endl;
			std::cin.get(); exit(1);
		}
		pakheader header;
		fread((void*)&header, sizeof(header), 1, file);

		//stats
		totalMaterials += header.no_of_materials;
		totalObjects += header.no_of_subobjects;

		//pak => mats
		for (int i = 0; i < header.no_of_materials; i++) {
			SceneMaterial *mat = new SceneMaterial();

			//create pak
			pakmaterial pak;
			//write pak header
			fread((void*)&pak, sizeof(pak), 1, file);
			mat->alpha = pak.alpha;
			mat->kad[0] = pak.kad[0];	mat->kad[1] = pak.kad[1];	mat->kad[2] = pak.kad[2];
			mat->ke[0] = pak.ke[0];	mat->ke[1] = pak.ke[1];	mat->ke[2] = pak.ke[2];
			mat->ks[0] = pak.ks[0];	mat->ks[1] = pak.ks[1];	mat->ks[2] = pak.ks[2];
			mat->shininess = pak.shininess;
			mat->alphamap_x = pak.alphamap_x;		mat->alphamap_y = pak.alphamap_y;
			mat->aomap_x = pak.aomap_x;				mat->aomap_y = pak.aomap_y;
			mat->detailmap_x = pak.detailmap_x;		mat->detailmap_y = pak.detailmap_y;
			mat->diffusemap_x = pak.diffusemap_x;	mat->diffusemap_y = pak.diffusemap_y;
			mat->displacementmap_x = pak.displacemap_x;		mat->displacementmap_y = pak.displacemap_y;
			mat->dudvmap_x = pak.dudvmap_x;			mat->dudvmap_y = pak.dudvmap_y;
			mat->emissivemap_x = pak.emissivemap_x;	mat->emissivemap_y = pak.emissivemap_y;
			mat->extramap_x = pak.extramap_x;		mat->extramap_y = pak.extramap_y;
			mat->normalmap_x = pak.normalmap_x;		mat->normalmap_y = pak.normalmap_y;
			mat->specularmap_x = pak.specularmap_x;	mat->specularmap_y = pak.specularmap_y;

			//name
			char *auxstr = (char*)malloc(sizeof(char)*(pak.namelength + 1));
			fread((void*)auxstr, pak.namelength, 1, file);
			auxstr[pak.namelength] = '\0';
			mat->name.assign(auxstr);
			free(auxstr);
			//alpha (luminance)
			int alphaSize = pak.alphamap_x*pak.alphamap_y;
			if (alphaSize > 0) mat->has_alphamap = true;
			mat->alphaData = (unsigned char*)malloc(sizeof(unsigned char)*alphaSize);
			fread((void*)mat->alphaData, alphaSize, 1, file);
			//ao (luminance)
			int aoSize = pak.aomap_x*pak.aomap_y;
			if (aoSize > 0) mat->has_aomap = true;
			mat->aoData = (unsigned char*)malloc(sizeof(unsigned char)*aoSize);
			fread((void*)mat->aoData, aoSize, 1, file);
			//detail (RGB)
			int detailSize = pak.detailmap_x*pak.detailmap_y * 3;
			if (detailSize > 0) mat->has_detailmap = true;
			mat->detailData = (unsigned char*)malloc(sizeof(unsigned char)*detailSize);
			fread((void*)mat->detailData, detailSize, 1, file);
			//diffuse (RGB)	
			int diffuseSize = pak.diffusemap_x*pak.diffusemap_y * 3;
			if (diffuseSize > 0) mat->has_diffusemap = true;
			mat->diffuseData = (unsigned char*)malloc(sizeof(unsigned char)*diffuseSize);
			fread((void*)mat->diffuseData, diffuseSize, 1, file);
			//displacement (luminance)
			int displacementSize = pak.displacemap_x*pak.displacemap_y;
			if (displacementSize > 0) mat->has_displacementmap = true;
			mat->displacementData = (unsigned char*)malloc(sizeof(unsigned char)*displacementSize);
			fread((void*)mat->displacementData, displacementSize, 1, file);
			//dudv (RGB)
			int dudvSize = pak.dudvmap_x*pak.dudvmap_y * 3;
			if (dudvSize > 0) mat->has_dudvmap = true;
			mat->dudvData = (unsigned char*)malloc(sizeof(unsigned char)*dudvSize);
			fread((void*)mat->dudvData, dudvSize, 1, file);
			//emissive (RGB)
			int emissiveSize = pak.emissivemap_x*pak.emissivemap_y * 3;
			if (emissiveSize > 0) mat->has_emissivemap = true;
			mat->emissiveData = (unsigned char*)malloc(sizeof(unsigned char)*emissiveSize);
			fread((void*)mat->emissiveData, emissiveSize, 1, file);
			//extra (RGB)
			int extraSize = pak.extramap_x*pak.extramap_y * 3;
			if (extraSize > 0) mat->has_extramap = true;
			mat->extraData = (unsigned char*)malloc(sizeof(unsigned char)*extraSize);
			fread((void*)mat->extraData, extraSize, 1, file);
			//normal (RGB)
			int normalSize = pak.normalmap_x*pak.normalmap_y * 3;
			if (normalSize > 0) mat->has_normalmap = true;
			mat->normalData = (unsigned char*)malloc(sizeof(unsigned char)*normalSize);
			fread((void*)mat->normalData, normalSize, 1, file);
			//specular (luminance)
			int specularSize = pak.specularmap_x*pak.specularmap_y;
			if (specularSize > 0) mat->has_specularmap = true;
			mat->specularData = (unsigned char*)malloc(sizeof(unsigned char)*specularSize);
			fread((void*)mat->specularData, specularSize, 1, file);

			mat->init();//frees malloc'd arrays

			//stats
			if (mat->has_alphamap) { totalTextures++; totalTextureSize += mat->alphamap_x*mat->alphamap_y; }
			if (mat->has_aomap) { totalTextures++; totalTextureSize += mat->aomap_x*mat->aomap_y; }
			if (mat->has_detailmap) { totalTextures++; totalTextureSize += mat->detailmap_x*mat->detailmap_y * 3; }
			if (mat->has_diffusemap) { totalTextures++; totalTextureSize += mat->diffusemap_x*mat->diffusemap_y * 3; }
			if (mat->has_displacementmap) { totalTextures++; totalTextureSize += mat->displacementmap_x*mat->displacementmap_y; }
			if (mat->has_dudvmap) { totalTextures++; totalTextureSize += mat->dudvmap_x*mat->dudvmap_y * 3; }
			if (mat->has_emissivemap) { totalTextures++; totalTextureSize += mat->emissivemap_x*mat->emissivemap_y * 3; }
			if (mat->has_extramap) { totalTextures++; totalTextureSize += mat->extramap_x*mat->extramap_y * 3; }
			if (mat->has_normalmap) { totalTextures++; totalTextureSize += mat->normalmap_x*mat->normalmap_y * 3; }
			if (mat->specularmap) { totalTextures++; totalTextureSize += mat->specularmap_x*mat->specularmap_y; }

			//add to scene
			sceneMaterials.push_back(mat);
		}
		//pak => obj
		for (int i = 0; i < header.no_of_subobjects; i++) {
			SceneObject *obj = new SceneObject();

			//create pak
			pakobject pak;
			//write header
			fread((void*)&pak, sizeof(pak), 1, file);
			obj->material_index = pak.material_index + oldmaterialcount;
			//name
			char *auxbuff = (char*)malloc(sizeof(char)*(pak.nameSize + 1));
			fread((void*)auxbuff, pak.nameSize, 1, file);
			auxbuff[pak.nameSize] = '\0';
			obj->name.assign(auxbuff);
			free(auxbuff);
			//verts
			obj->vertices.resize(pak.verticesSize);
			fread((void*)&obj->vertices[0], pak.verticesSize * 3 * sizeof(float), 1, file);
			//normals
			obj->normals.resize(pak.normalsSize);
			fread((void*)&obj->normals[0], pak.normalsSize * 3 * sizeof(float), 1, file);
			//tangents
			obj->tangents.resize(pak.tangentsSize);
			fread((void*)&obj->tangents[0], pak.tangentsSize * 4 * sizeof(float), 1, file);
			//texcoords
			obj->texcoords.resize(pak.texcoordsSize);
			fread((void*)&obj->texcoords[0], pak.texcoordsSize * 2 * sizeof(float), 1, file);
			//indices
			obj->indices.resize(pak.indicesSize);
			fread((void*)&obj->indices[0], pak.indicesSize * sizeof(unsigned int), 1, file);

			//stats
			totalVertices += pak.verticesSize * 3;
			totalTriangles += pak.indicesSize / 3;

			obj->init();//frees
			sceneObjects.push_back(obj);
		}
		fclose(file);

		//sort it by material
		for (unsigned int i = 0; i < sceneObjects.size() - 1; i++) {
			for (unsigned int j = i + 1; j < sceneObjects.size(); j++) {
				if (sceneObjects[i]->material_index > sceneObjects[j]->material_index) {
					SceneObject* aux = sceneObjects[i];
					sceneObjects[i] = sceneObjects[j];
					sceneObjects[j] = aux;
				}
			}
		}

		//create samplers
		glGenSamplers(1, &samplerTrilinearAnisotropic);
		glSamplerParameteri(samplerTrilinearAnisotropic, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(samplerTrilinearAnisotropic, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameteri(samplerTrilinearAnisotropic, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(samplerTrilinearAnisotropic, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(samplerTrilinearAnisotropic, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);

		glGenSamplers(1, &samplerTrilinear);
		glSamplerParameteri(samplerTrilinear, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(samplerTrilinear, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameteri(samplerTrilinear, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(samplerTrilinear, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glGenSamplers(1, &samplerBilinear);
		glSamplerParameteri(samplerBilinear, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(samplerBilinear, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameteri(samplerBilinear, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(samplerBilinear, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		setFilteringTrilinearAnisotropic();

		double clockend = std::clock();
		std::cout << "Scene loading time is " << (clockend - clockstart) / CLOCKS_PER_SEC << std::endl;

		std::cout << "*********************************************************************" << std::endl;
		return true;
	}

	// various draws
	void Scene::drawScene(Shader& shader, bool withmaterial) {
		if (withmaterial) setFilteringState(currentSampler);
		int lastmaterial = -1;
		for (unsigned int i = 0; i < sceneObjects.size(); i++) {
			if (withmaterial) {
				int currentmaterial = sceneObjects[i]->material_index;
				if (currentmaterial != lastmaterial) {
					sceneMaterials[currentmaterial]->use(shader);
					lastmaterial = currentmaterial;
				}
			}
			sceneObjects[i]->draw();
		}
	}
	void Scene::drawScenePatches(Shader& shader, bool withmaterial) {
		if (withmaterial) setFilteringState(currentSampler);
		int lastmaterial = -1;
		for (unsigned int i = 0; i < sceneObjects.size(); i++) {
			if (withmaterial) {
				int currentmaterial = sceneObjects[i]->material_index;
				if (currentmaterial != lastmaterial) {
					sceneMaterials[currentmaterial]->use(shader);
					lastmaterial = currentmaterial;
				}
			}
			sceneObjects[i]->drawPatches();
		}
	}
	void Scene::drawSingleObject(Shader& shader, unsigned int index, bool withmaterial) {
		if (index < 0 || index >= sceneObjects.size()) return;
		if (withmaterial) {
			setFilteringState(currentSampler);
			sceneMaterials[sceneObjects[index]->material_index]->use(shader);
		}
		sceneObjects[index]->draw();
	}
	std::string Scene::getVertices() { 
		return std::to_string(totalVertices); 
	}
	std::string Scene::getTriangles() { 
		return std::to_string(totalTriangles); 
	}
	std::string Scene::getObjects() {
		return std::to_string(totalObjects); 
	}
	std::string Scene::getMaterials() { 
		return std::to_string(totalMaterials); 
	}
	std::string Scene::getTextures() { 
		return std::to_string(totalTextures); 
	}
	std::string Scene::getTextureSize() { 
		return std::to_string(totalTextureSize); 
	}
	std::string Scene::getFPS() {
		double ms = glm::max((double)timer.ElapsedSinceLastCallSeconds(),0.0000000000001);
		return std::to_string(1.0f / ms);
	}
	void Scene::setFilteringBilinear() {
		setFilteringState(samplerBilinear); 
	}
	void Scene::setFilteringTrilinear() {
		setFilteringState(samplerTrilinear); 
	}
	void Scene::setFilteringTrilinearAnisotropic() {
		setFilteringState(samplerTrilinearAnisotropic); 
	}
	void Scene::setFilteringState(int samplerid) {
		currentSampler = samplerid;
		for (unsigned int i = 0; i < 10; i++)	glBindSampler(i, samplerid);
	}

}