#include <chrono>

namespace rgp {
	//------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------
	//timer
	class Timer {
	public:
		Timer();
		void Reset();
		int64_t Elapsed();
		int64_t ElapsedSinceLastCall();
		float ElapsedSeconds();
		float ElapsedMilliSeconds();
		float ElapseMicroSeconds();
		float ElapsedSinceLastCallSeconds();
		float ElapsedSinceLastCallMilliSeconds();
		float ElapsedSinceLastCallMicroSeconds();
	private:
		std::chrono::high_resolution_clock::time_point mCurrent;
		std::chrono::high_resolution_clock::time_point mStart;
	};

}