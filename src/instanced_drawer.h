#pragma once
#include "dependencies.h"
#include "scene.h"

namespace rgp {
	class InstancedDrawer {
	public:
		InstancedDrawer();
		~InstancedDrawer();

		bool load(std::string assetpath, int instances);
		unsigned int getTextureBuffer();
		int getInstanceCount();
		void drawPatches(Shader& shader, bool withmaterial);
	public:
		Scene scene;
		unsigned int bufferid =0, texturebufferid =0;
		int instancecount =0;
	};
}