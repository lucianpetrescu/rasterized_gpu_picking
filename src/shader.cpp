
#include "shader.h"
#include <iostream>

namespace rgp {
	const unsigned int UNINITIALIZED = 0;
	const unsigned int SHADER_TYPE_C = 80;
	const unsigned int SHADER_TYPE_V = 81;
	const unsigned int SHADER_TYPE_VF = 82;
	const unsigned int SHADER_TYPE_VGF = 83;
	const unsigned int SHADER_TYPE_VTTF = 84;
	const unsigned int SHADER_TYPE_VTTGF = 85;

	//--------------------------------------------------------------------------------------------
	Shader::Shader() {
		shader = UNINITIALIZED; type = UNINITIALIZED;
	}
	Shader::Shader(const std::string& compute_file) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(compute_file);
	}
	Shader::Shader(const std::string& vertex_file, bool nameoverload) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(vertex_file, true);
	}
	Shader::Shader(const std::string& vertex_file, const std::string& fragment_file) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(vertex_file, fragment_file);
	}
	Shader::Shader(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(vertex_file, geometry_file, fragment_file);
	}
	Shader::Shader(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(vertex_file, tess_ctrl_file, tess_eval_file, fragment_file);
	}
	Shader::Shader(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file) {
		shader = UNINITIALIZED; type = UNINITIALIZED;
		Set(vertex_file, tess_ctrl_file, tess_eval_file, geometry_file, fragment_file);
	}
	void Shader::Set(const std::string& compute_file) {
		type = SHADER_TYPE_C;
		this->compute_file = compute_file;
		Reload();
	}
	void Shader::Set(const std::string& vertex_file, bool nameoverload) {
		type = SHADER_TYPE_V;
		this->vertex_file = vertex_file;
		Reload();
	}
	void Shader::Set(const std::string& vertex_file, const std::string& fragment_file) {
		type = SHADER_TYPE_VF;
		this->vertex_file = vertex_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void Shader::Set(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file) {
		type = SHADER_TYPE_VGF;
		this->vertex_file = vertex_file;
		this->geometry_file = geometry_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void Shader::Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file) {
		type = SHADER_TYPE_VTTF;
		this->vertex_file = vertex_file;
		this->tess_ctrl_file = tess_ctrl_file;
		this->tess_eval_file = tess_eval_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void Shader::Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file) {
		type = SHADER_TYPE_VTTGF;
		this->vertex_file = vertex_file;
		this->tess_ctrl_file = tess_ctrl_file;
		this->tess_eval_file = tess_eval_file;
		this->geometry_file = geometry_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	Shader::~Shader() {
		if (shader != UNINITIALIZED) glDeleteProgram(shader);
	}
	void Shader::Reload() {
		//rebuild
		if (shader != UNINITIALIZED) glDeleteProgram(shader);
		if (type == SHADER_TYPE_C)     shader = _LoadShader(compute_file);
		else if (type == SHADER_TYPE_V)     shader = _LoadShader(vertex_file, true);
		else if (type == SHADER_TYPE_VF)    shader = _LoadShader(vertex_file, fragment_file);
		else if (type == SHADER_TYPE_VGF)   shader = _LoadShader(vertex_file, geometry_file, fragment_file);
		else if (type == SHADER_TYPE_VTTF)  shader = _LoadShader(vertex_file, tess_ctrl_file, tess_eval_file, fragment_file);
		else if (type == SHADER_TYPE_VTTGF) shader = _LoadShader(vertex_file, tess_ctrl_file, tess_eval_file, geometry_file, fragment_file);
		//clear locations as they can't be valid any more
		uniform_locations.clear();
	}
	unsigned int Shader::GetType() { return type; }

	void Shader::SetUniform(const std::string& name, int value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1i(location, value);
	}
	void Shader::SetUniform(const std::string& name, unsigned int value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1ui(location, value);
	}
	void Shader::SetUniform(const std::string& name, float value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1f(location, value);
	}
	void Shader::SetUniform(const std::string& name, const glm::vec2& value) {
		SetUniform(name, value.x, value.y);
	}
	void Shader::SetUniform(const std::string& name, float value0, float value1) {
		int location = _GetLocation(name);
		if (location != -1) glUniform2f(location, value0, value1);
	}
	void Shader::SetUniform(const std::string& name, const glm::vec3& value) {
		SetUniform(name, value.x, value.y, value.z);
	}
	void Shader::SetUniform(const std::string& name, float value0, float value1, float value2) {
		int location = _GetLocation(name);
		if (location != -1) glUniform3f(location, value0, value1, value2);
	}
	void Shader::SetUniform(const std::string& name, const glm::vec4& value) {
		SetUniform(name, value.x, value.y, value.z, value.w);
	}
	void Shader::SetUniform(const std::string& name, float value0, float value1, float value2, float value3) {
		int location = _GetLocation(name);
		if (location != -1) glUniform4f(location, value0, value1, value2, value3);
	}
	void Shader::SetUniform(const std::string& name, const glm::mat4& m, bool transpose) {
		int location = _GetLocation(name);
		if (location != -1) glUniformMatrix4fv(location, 1, transpose, glm::value_ptr(m));
	}
	void Shader::Bind() {
		glUseProgram(shader);
	}
	unsigned int Shader::GetGLShader() { return shader; }
	int Shader::_GetLocation(const std::string& name) {
		//exists?
		auto it = uniform_locations.find(name);
		if (it != uniform_locations.end()) return it->second;

		//if not, add
		int location = glGetUniformLocation(shader, name.c_str());
		if (location == -1) std::cout << "[Shader] WARNING, variable " << name << " does not exist or it was optimzed out by the compiler because it is not used." << std::endl;
		uniform_locations[name] = location;		//add even if not found (-1) in order to not produce the same message N times
		return location;
	}


	unsigned int Shader::_LoadShader(const std::string &compute_shader_file) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tcompute shader = " << compute_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(compute_shader_file, GL_COMPUTE_SHADER));
		return _CreateProgram(shaders);
	}
	unsigned int Shader::_LoadShader(const std::string &vertex_shader_file, bool nameoverload) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tvertex shader = " << vertex_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(vertex_shader_file, GL_VERTEX_SHADER));
		return _CreateProgram(shaders);
	}
	unsigned int Shader::_LoadShader(const std::string &vertex_shader_file, const std::string &fragment_shader_file) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tvertex shader = " << vertex_shader_file << std::endl;
		std::cout << "\tfragment shader = " << fragment_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(vertex_shader_file, GL_VERTEX_SHADER));
		shaders.push_back(_CreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
		return _CreateProgram(shaders);
	}
	unsigned int Shader::_LoadShader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tvertex shader = " << vertex_shader_file << std::endl;
		std::cout << "\tgeometry shader = " << geometry_shader_file << std::endl;
		std::cout << "\tfragment shader = " << fragment_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(vertex_shader_file, GL_VERTEX_SHADER));
		shaders.push_back(_CreateShader(geometry_shader_file, GL_GEOMETRY_SHADER));
		shaders.push_back(_CreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
		return _CreateProgram(shaders);
	}
	unsigned int Shader::_LoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string &fragment_shader_file) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tvertex shader = " << vertex_shader_file << std::endl;
		std::cout << "\tfragment shader = " << fragment_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(vertex_shader_file, GL_VERTEX_SHADER));
		shaders.push_back(_CreateShader(tessctrl_shader_file, GL_TESS_CONTROL_SHADER));
		shaders.push_back(_CreateShader(tesseval_shader_file, GL_TESS_EVALUATION_SHADER));
		shaders.push_back(_CreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
		return _CreateProgram(shaders);
	}
	unsigned int Shader::_LoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file) {
		std::cout << "[Shader] loading: " << std::endl;
		std::cout << "\tvertex shader = " << vertex_shader_file << std::endl;
		std::cout << "\tfragment shader = " << fragment_shader_file << std::endl;
		std::vector<unsigned int> shaders;
		shaders.push_back(_CreateShader(vertex_shader_file, GL_VERTEX_SHADER));
		shaders.push_back(_CreateShader(tessctrl_shader_file, GL_TESS_CONTROL_SHADER));
		shaders.push_back(_CreateShader(tesseval_shader_file, GL_TESS_EVALUATION_SHADER));
		shaders.push_back(_CreateShader(geometry_shader_file, GL_GEOMETRY_SHADER));
		shaders.push_back(_CreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
		return _CreateProgram(shaders);
	}

	unsigned int Shader::_CreateShader(const std::string &shader_file, GLenum shader_type) {
		std::string shader_code;
		std::ifstream file(shader_file.c_str(), std::ios::in);
		if (!file.good()) {
			std::cout << "[Shader] Could not find file " << shader_file << " or lacking reading rights!" << std::endl;
			std::terminate();
		}

		while (file.good()) {
			std::string line;
			std::getline(file, line);
			shader_code.append(line + "\n");
		}
		file.close();


		int info_log_length = 0, compile_result = 0;
		unsigned int gl_shader_object;

		gl_shader_object = glCreateShader(shader_type);
		const char *shader_code_ptr = shader_code.c_str();
		const int shader_code_size = (int)shader_code.size();
		glShaderSource(gl_shader_object, 1, &shader_code_ptr, &shader_code_size);
		glCompileShader(gl_shader_object);
		glGetShaderiv(gl_shader_object, GL_COMPILE_STATUS, &compile_result);


		if (compile_result == GL_FALSE) {
			std::string str_shader_type = "";
			if (shader_type == GL_VERTEX_SHADER) str_shader_type = "vertex shader";
			if (shader_type == GL_TESS_CONTROL_SHADER) str_shader_type = "tess control shader";
			if (shader_type == GL_TESS_EVALUATION_SHADER) str_shader_type = "tess evaluation shader";
			if (shader_type == GL_GEOMETRY_SHADER) str_shader_type = "geometry shader";
			if (shader_type == GL_FRAGMENT_SHADER) str_shader_type = "fragment shader";
			if (shader_type == GL_COMPUTE_SHADER) str_shader_type = "compute shader";

			glGetShaderiv(gl_shader_object, GL_INFO_LOG_LENGTH, &info_log_length);
			std::vector<char> shader_log(info_log_length);
			glGetShaderInfoLog(gl_shader_object, info_log_length, NULL, &shader_log[0]);
			std::cout << "[Shader] COMPILE ERROR in " << str_shader_type << std::endl << &shader_log[0] << std::endl;
			return 0;
		}

		return gl_shader_object;
	}
	unsigned int Shader::_CreateProgram(const std::vector<unsigned int> &shader_objects) {
		int info_log_length = 0, link_result = 0;

		unsigned int gl_program_object = glCreateProgram();
		for (std::vector<unsigned int>::const_iterator it = shader_objects.begin(); it != shader_objects.end(); it++) glAttachShader(gl_program_object, (*it));
		glLinkProgram(gl_program_object);
		glGetProgramiv(gl_program_object, GL_LINK_STATUS, &link_result);

		if (link_result == GL_FALSE) {
			glGetProgramiv(gl_program_object, GL_INFO_LOG_LENGTH, &info_log_length);
			std::vector<char> program_log(info_log_length);
			glGetProgramInfoLog(gl_program_object, info_log_length, NULL, &program_log[0]);
			std::cout << "[Shader] LINK ERROR:" << std::endl << &program_log[0] << std::endl;
			return 0;
		}

		for (std::vector<unsigned int>::const_iterator it = shader_objects.begin(); it != shader_objects.end(); it++) glDeleteShader((*it));

		return gl_program_object;
	}
}