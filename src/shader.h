#pragma once
#include "dependencies.h"
#include <fstream>
#include <string>
#include <iostream>

namespace rgp {
	class Shader{
	public:
		Shader();
		Shader(const std::string& compute_file);
		Shader(const std::string& vertex_file, bool nameoverload);
		Shader(const std::string& vertex_file, const std::string& fragment_file);
		Shader(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file);
		Shader(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file);
		Shader(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file);
		void Set(const std::string& compute_file);
		void Set(const std::string& vertex_file, bool nameoverload);
		void Set(const std::string& vertex_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file);
		~Shader();
		void Reload();
		unsigned int GetType();

		void SetUniform(const std::string& name, int value);
		void SetUniform(const std::string& name, unsigned int value);
		void SetUniform(const std::string& name, float value);
		void SetUniform(const std::string& name, const glm::vec2& value);
		void SetUniform(const std::string& name, float value0, float value1);
		void SetUniform(const std::string& name, const glm::vec3& value);
		void SetUniform(const std::string& name, float value0, float value1, float value2);
		void SetUniform(const std::string& name, const glm::vec4& value);
		void SetUniform(const std::string& name, float value0, float value1, float value2, float value3);
		void SetUniform(const std::string& name, const glm::mat4& m, bool transpose);
		void Bind();
		unsigned int GetGLShader();
	private:

		unsigned int _LoadShader(const std::string &compute_shader_file);
		unsigned int _LoadShader(const std::string &vertex_shader_file, bool nameoverload);
		unsigned int _LoadShader(const std::string &vertex_shader_file, const std::string &fragment_shader_file);
		unsigned int _LoadShader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file);
		unsigned int _LoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string &fragment_shader_file);
		unsigned int _LoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file);
		unsigned int _CreateShader(const std::string &shader_file, GLenum shader_type);
		unsigned int _CreateProgram(const std::vector<unsigned int> &shader_objects);
		int _GetLocation(const std::string& name);

	private:
		std::map<std::string, int> uniform_locations;
		unsigned int shader;
		unsigned int type;
		std::string compute_file;
		std::string vertex_file;
		std::string fragment_file;
		std::string geometry_file;
		std::string tess_ctrl_file;
		std::string tess_eval_file;
	};
}