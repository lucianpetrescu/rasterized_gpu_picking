#pragma once
#include "dependencies.h"
#include "shader.h"
#include "timer.h"
#include <ctime>
#include <fstream>

namespace rgp {
	///pak headers -----------------------------------------------------------------------------------
	struct pakmaterial {
		int namelength;
		float kad[3], ke[3], ks[3];
		float shininess;
		float alpha;
		int aomap_x;
		int aomap_y;
		int alphamap_x;
		int alphamap_y;
		int detailmap_x;
		int detailmap_y;
		int diffusemap_x;
		int diffusemap_y;
		int displacemap_x;
		int displacemap_y;
		int dudvmap_x;
		int dudvmap_y;
		int emissivemap_x;
		int emissivemap_y;
		int extramap_x;
		int extramap_y;
		int normalmap_x;
		int normalmap_y;
		int specularmap_x;
		int specularmap_y;
	};


	struct pakobject {
		int nameSize;
		int verticesSize;
		int normalsSize;
		int tangentsSize;
		int texcoordsSize;
		int indicesSize;
		int material_index;
	};
	struct pakheader {
		int no_of_subobjects;
		int no_of_materials;
	};
	

	//material  -----------------------------------------------------------------------------------
	class SceneMaterial {
	public:
		SceneMaterial();
	private:
		void generateTexture(unsigned int *map, int map_x, int map_y, unsigned char *data, bool threebytes);
	public:
		void init();

	private:
		void bindtexture(int target, unsigned int map, int loc);
	public:
		void use(Shader& shader);
		~SceneMaterial();
	public:
		std::string name;			//mat name
		float shininess;
		float alpha;
		float kad[3];
		float ks[3];
		float ke[3];
		int aomap_x, aomap_y, alphamap_x, alphamap_y, diffusemap_x, diffusemap_y, displacementmap_x, displacementmap_y, detailmap_x, detailmap_y;
		int dudvmap_x, dudvmap_y, emissivemap_x, emissivemap_y, extramap_x, extramap_y, normalmap_x, normalmap_y, specularmap_x, specularmap_y;
		bool has_aomap, has_alphamap, has_detailmap, has_diffusemap, has_displacementmap, has_dudvmap, has_emissivemap, has_extramap, has_normalmap, has_specularmap;
		unsigned char *aoData, *alphaData, *detailData, *diffuseData, *displacementData, *dudvData, *emissiveData, *extraData, *normalData, *specularData;
		unsigned int aomap, alphamap, detailmap, diffusemap, displacementmap, dudvmap, emissivemap, extramap, normalmap, specularmap;
		//faster usage
		int programlast;
		int loc[15];
	};

	//obiect de scena  -----------------------------------------------------------------------------------
	class SceneObject {
	public:
		SceneObject();
		void init();
		void draw();
		void drawPatches();
		~SceneObject();
	public:
		unsigned int vao;
		unsigned int verticesvbo;
		unsigned int normalsvbo;
		unsigned int tangentsvbo;
		unsigned int texcoordsvbo;
		unsigned int ibo;
		int indicestodraw;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec4> tangents;
		std::vector<glm::vec2> texcoords;
		std::vector<unsigned int> indices;
		std::string name;
		int material_index;
	};


	// scene loader  -----------------------------------------------------------------------------------
	class Scene {
	public:
		Scene();
		~Scene();
		bool load(std::string scenePakFile);
		void drawScene(Shader& shader, bool withmaterial);
		void drawScenePatches(Shader& shader, bool withmaterial);
		void drawSingleObject(Shader& shader, unsigned int index, bool withmaterial);
		void setFilteringBilinear();
		void setFilteringTrilinear();
		void setFilteringTrilinearAnisotropic();
		std::string getVertices();
		std::string getTriangles();
		std::string getObjects();
		std::string getMaterials();
		std::string getTextures();
		std::string getTextureSize();
		std::string getFPS();
	private:
		void setFilteringState(int samplerid);
	public:
		Timer timer;
		unsigned int samplerBilinear;
		unsigned int samplerTrilinear;
		unsigned int samplerTrilinearAnisotropic;
		unsigned int currentSampler;
		int totalVertices;
		int totalTriangles;
		int totalObjects;
		int totalMaterials;
		int totalTextures;
		int totalTextureSize;
		std::vector<SceneObject*> sceneObjects;
		std::vector<SceneMaterial*> sceneMaterials;
	};
}