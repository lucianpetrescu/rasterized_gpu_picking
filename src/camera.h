//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#pragma once
#include "dependencies.h"


namespace rgp {
	//simple FPS camera
	class Camera {
	public:
		Camera();
		Camera(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up);
		~Camera();

		void setView(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up);

		void translateForward(float distance);
		void translateUpword(float distance);
		void translateRight(float distance);

		void rotateX(float angle);
		void rotateY(float angle);

		const glm::mat4& getViewMatrix();

		const glm::vec3& getPosition();
		const glm::vec3& getUp();
		const glm::vec3& getForward();
		const glm::vec3 getFocus();
		const glm::vec3& getRight();

	private:
		void computeView();
	private:
		glm::vec3 position;
		glm::vec3 up;
		glm::vec3 forward;
		glm::vec3 right;
		glm::mat4 view_matrix;
	};
}