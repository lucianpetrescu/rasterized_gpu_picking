#include "picking_buffer.h"

namespace rgp {
	PickingBuffer::PickingBuffer() {
	}
	PickingBuffer::~PickingBuffer() {
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 4, 0);
		if (pickBuffer != 0) glDeleteBuffers(1, &pickBuffer);
		if (atomicbuffer != 0) glDeleteBuffers(1, &atomicbuffer);
	}

	void PickingBuffer::init(int count) {
		//create shader storage for data
		if (pickBuffer != 0) glDeleteBuffers(1, &pickBuffer);
		glGenBuffers(1, &pickBuffer);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, pickBuffer);
		glBufferData(GL_SHADER_STORAGE_BUFFER, 16000000, NULL, GL_DYNAMIC_DRAW);
		memory_used = 0;

		//create atomic buffer
		if (atomicbuffer != 0) glDeleteBuffers(1, &atomicbuffer);
		glGenBuffers(1, &atomicbuffer);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 4, atomicbuffer);
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, 2 * sizeof(GLuint), 0, GL_DYNAMIC_DRAW);

		//initialize atomic counter buffer
		GLuint* ptr = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER, 0, 2 * sizeof(GLuint), GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
		if (ptr) {
			ptr[0] = 0;
			ptr[1] = 0;
			memory_used = ptr[0] * 32;
		}
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);

		//clear fragments
		std::memset(&fragments[0], 0, 1000 * sizeof(PickingFragment));
		fragment_count_solid = fragment_count_transparent = 0;
	}

	void PickingBuffer::bindForReadWrite(unsigned int unitPickBuffer, unsigned int unitAtomicBuffer) {
		//data
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, unitPickBuffer, pickBuffer);
		//atomic
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, unitAtomicBuffer, atomicbuffer);
	}

	void PickingBuffer::getData() {
		//get size and reinit atomic counter
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicbuffer);
		GLuint* ptr = (GLuint*)glMapBufferRange(GL_ATOMIC_COUNTER_BUFFER, 0, 2 * sizeof(GLuint), GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);
		if (ptr) {
			fragment_count_transparent = (int)ptr[1];
			fragment_count_solid = (int)ptr[0] - (int)ptr[1];
			memory_used = ptr[0] * 32;
			ptr[0] = 0;
			ptr[1] = 0;
		}
		else {
			fragment_count_transparent = 0;
			fragment_count_solid = 0;
			memory_used = 0;
		}
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);

		if (memory_used == 0) {
			memset(&fragments[0], 0, 2 * sizeof(PickingFragment));
		}

		//get data from buffer
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, pickBuffer);
		if (memory_used > 0) {
			PickingFragment* buffptr = (PickingFragment*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, memory_used, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);
			if (buffptr) {
				//if we selected something
				std::memcpy(&fragments[0], buffptr, glm::min(memory_used, 100 * (int)sizeof(PickingFragment)));
				if (fragment_count_solid == 0) { buffptr[0].objectId = 0;  memset(&fragments[0], 0, sizeof(PickingFragment)); }
				if (fragment_count_transparent == 0) { buffptr[1].objectId = 0; memset(&fragments[1], 0, sizeof(PickingFragment)); }
			}
			glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		}
		else {
			PickingFragment* buffptr = (PickingFragment*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 2*sizeof(PickingFragment), GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);
			std::memcpy(buffptr, &fragments[0], 2*sizeof(PickingFragment));
			glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		}
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}

	int PickingBuffer::getFragmentCountSolid() { 
		return fragment_count_solid;
	}
	int PickingBuffer::getFragmentCountTransparent() { 
		return fragment_count_transparent; 
	}
	int PickingBuffer::getMemoryUsed() { 
		return memory_used; 
	}
}