#include "instanced_drawer.h"

namespace rgp {
	InstancedDrawer::InstancedDrawer() {
	}
	InstancedDrawer::~InstancedDrawer() {
		if (bufferid != 0) glDeleteBuffers(1, &bufferid);
		if (texturebufferid != 0)glDeleteTextures(1, &texturebufferid);
	}

	bool InstancedDrawer::load(std::string assetpath, int instances) {
		instancecount = instances;
		bool res = scene.load(assetpath);

		int totalsize = instances * 4 * sizeof(float);
		float *data = new float[totalsize];		//(px +py+pz+radius) + (cx+cy+cz+att)
		srand((unsigned int)std::clock());
		for (int i = 0; i < instances; i++) {
			float px = (float)(rand() % 100);
			float pz = (float)(rand() % 100);
			float rot = (float)(rand() % 360 * 3.1416 / 180.0f);
			float scale = (rand() % 100 + 150) / 200.0f;

			data[i * 4 + 0] = px;
			data[i * 4 + 1] = pz;
			data[i * 4 + 2] = rot;
			data[i * 4 + 3] = scale;
		}

		//create buffer
		if (bufferid != 0) glDeleteBuffers(1, &bufferid);
		glGenBuffers(1, &bufferid);
		glBindBuffer(GL_TEXTURE_BUFFER, bufferid);
		glBufferData(GL_TEXTURE_BUFFER, totalsize, data, GL_STATIC_DRAW);
		delete[] data;

		//associate buffer with a texture
		if (texturebufferid != 0) glDeleteTextures(1, &texturebufferid);
		glGenTextures(1, &texturebufferid);
		glBindTexture(GL_TEXTURE_BUFFER, texturebufferid);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, bufferid);
		//unbind
		glBindBuffer(GL_TEXTURE_BUFFER,0);

		return res;
	}
	unsigned int InstancedDrawer::getTextureBuffer() {
		return texturebufferid;
	}
	int InstancedDrawer::getInstanceCount() {
		return instancecount;
	}
	void InstancedDrawer::drawPatches(Shader& shader, bool withmaterial) {
		if (withmaterial) scene.sceneMaterials[0]->use(shader);

		//draw
		glBindVertexArray(scene.sceneObjects[0]->vao);
		glDrawElementsInstanced(GL_PATCHES, scene.sceneObjects[0]->indicestodraw, GL_UNSIGNED_INT, (void*)0, instancecount);
	}
}