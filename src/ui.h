#pragma once
#include "shader.h"
#include <string>

namespace rgp {
	class UIText {
	public:
		int startposx, startposy;
		int w, h;
		std::string text;
	};

	class UI {
	public:
		UI();
		~UI();
		void init(const std::string& fontfile);
		void reshape(int w, int h);
		void clearText();
		void addText(int startposx, int startposy, int wperchar, int hperchar, const std::string& text);
		void render();
		void reload();
	public:
		float screenwidth =0, screenheight =0;
		Shader shader;
		unsigned int uitexture =0;
		unsigned int uisampler =0;
		unsigned int uitexturelocation =0;
		unsigned int uivao =0;
		unsigned int uivertices =0;
		unsigned int uitexcoords =0;
		std::vector<UIText> texts;
		std::vector<float> vertices;
		std::vector<float> texcoords;
	};
}