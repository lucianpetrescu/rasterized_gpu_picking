//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
#include "camera.h"
#include "scene.h"
#include "shader.h"
#include "ui.h"
#include "particle_system.h"
#include "instanced_drawer.h"
#include "picking_buffer.h"
#include <ctime>

class App {
public:
	App(lap::wic::Window& wnd) {
		//enable multisampling
		glEnable(GL_MULTISAMPLE);

		//versiuni, renderer
		const unsigned char* renderer = glGetString(GL_RENDERER);
		const unsigned char* vendor = glGetString(GL_VENDOR);
		const unsigned char* version = glGetString(GL_VERSION);
		const unsigned char* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
		GLint major, minor;
		glGetIntegerv(GL_MAJOR_VERSION, &major);
		glGetIntegerv(GL_MINOR_VERSION, &minor);
		std::cout << "GL Vendor : " << vendor << std::endl;
		std::cout << "GL Renderer : " << renderer << std::endl;
		std::cout << "GL Version (string) : " << version << std::endl;
		std::cout << "GL Version (integer) : " << major << "." << minor << std::endl;
		std::cout << "GLSL Version : " << glslVersion << std::endl;

		//init: shaders and scene
		shader.Set(std::string("../shaders/Scene.vert"), std::string("../shaders/Scene.tcs"), std::string("../shaders/Scene.tes"), std::string("../shaders/Scene.frag"));

		//camera
		camera.setView(glm::vec3(8000, 2000, 8000), glm::vec3(0, 2000, 0), glm::vec3(0, 1, 0));
		//camera.setCamera(vector3(0,3000,0),vector3(0,1000,0),vector3(1,0,0));

		//ui
		ui.init(std::string("../assets/font.font"));

		//light
		light_position = glm::vec3(2200, 2500, 0);

		//ground
		ground.load(std::string("../assets/sandtess.pak"));
		
		//particle sys
		particle_system.create(250, 1500, 100, 900, 10);	//texturi & shadere in clasa
		particle_system.addPerturbator(rgp::ParticlePerturbator(0, 0, 0, 500, 1, 0, 0, 0.25f));			//right
		particle_system.addPerturbator(rgp::ParticlePerturbator(1000, 0, 0, 500, 1, 0, 0, -0.5f));		//left
		particle_system.addPerturbator(rgp::ParticlePerturbator(1000, 0, 0, 500, 1, 0, 0.2f, 0));		//up
		particle_system.addPerturbator(rgp::ParticlePerturbator(500, 0, 0, 500, 2, 0.1f, 0, 0));		//to original camera

																						//instanced palms
		tree.load(std::string("../assets/palmtree.pak"), 100);

		//picking
		picking_buffer.init(50);

		//viewport
		resize(wnd, wnd.getFramebufferProperties().width, wnd.getFramebufferProperties().height, 0);
	}
	~App() {
	}

	//----------------------------------------------------------------------------------------------------
	// SCENE ---------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------
	void renderScene() {
		//uniforms for the entire scene
		shader.Bind();
		shader.SetUniform("projMatrix", ProjectionMatrix, false);
		shader.SetUniform("viewMatrix", camera.getViewMatrix(), false);
		shader.SetUniform("Light.position", light_position);
		shader.SetUniform("Light.color", glm::vec3(1, 1, 1));
		shader.SetUniform("alpha_culling_enable", (int)toggle_alpha_culling);

		//picking uniforms for entire scene 
		if (picking_now) shader.SetUniform("picking_now", (float)1.f);
		else shader.SetUniform("picking_now", (float)0.f);
		if (picking_show) shader.SetUniform("picking_show", (float)1.f);
		else shader.SetUniform("picking_show", (float)0.f);
		shader.SetUniform("picking_xy", picking_x, picking_y);

		//ground
		shader.SetUniform("TessLevelOuter", toggle_tess_steps_outer);
		shader.SetUniform("TessLevelInner", toggle_tess_steps_inner);
		shader.SetUniform("displacement_height", (float)40.f);
		shader.SetUniform("isinstanced", (float)0.f);
		for (int i = 0; i<5; i++) {
			for (int j = 0; j<5; j++) {
				//id ground
				shader.SetUniform("picking_id", (int)(i * 5 + j + 1));
				//draw ground
				//ModelMatrix = glm::translate(glm::scale(glm::mat4(1), glm::vec3(2, 2, 2)), 2.f*glm::vec3(i*2000.0f, 0, j*2000.0f));
				ModelMatrix = glm::translate(glm::scale(glm::mat4(1),glm::vec3(2,2,2)), glm::vec3(i*2000.0f, 0, j*2000.0f));
				shader.SetUniform("modelMatrix", ModelMatrix, false);
				ground.drawScenePatches(shader, true);
			}
		}


		//trees
		//pick instanced
		shader.SetUniform("picking_id", (int)26);
		shader.SetUniform("TessLevelOuter", (int)2);
		shader.SetUniform("TessLevelInner", (int)2);
		shader.SetUniform("displacement_height", (float)0.01f);
		shader.SetUniform("isinstanced", (float)1.f);
		glActiveTexture(GL_TEXTURE10);
		glBindTexture(GL_TEXTURE_BUFFER, tree.getTextureBuffer());
		shader.SetUniform("dataBuffer", (int)10);
		//draw instances
		ModelMatrix = glm::scale(glm::mat4(1), glm::vec3(100, 100, 100));
		shader.SetUniform("modelMatrix", ModelMatrix, false);
		tree.drawPatches(shader, true);
	}

	//----------------------------------------------------------------------------------------------------
	// PARTICLES -----------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------
	void renderParticleSystems() {
		//simulate
		if (toggle_animation) particle_system.simulate(true);

		rgp::Shader& shaderParticles = particle_system.getProgramRender();
		shaderParticles.Bind();

		//picking uniforms
		shaderParticles.SetUniform("picking_id", (int)30);
		if(picking_now) shaderParticles.SetUniform("picking_now", (float)1.f);
		else shaderParticles.SetUniform("picking_now", (float)0.f);
		if(picking_show) shaderParticles.SetUniform("picking_show", (float)1.f);
		else shaderParticles.SetUniform("picking_show", (float)0.f);
		shaderParticles.SetUniform("picking_xy", picking_x, picking_y);

		//draw
		ModelMatrix = glm::translate(glm::mat4(1), glm::vec3(3500, 0, 3500));
		shaderParticles.SetUniform("modelMatrix", ModelMatrix, false);
		glm::mat4 ParticleModelMatrix = glm::mat4(1);
		particle_system.render(ParticleModelMatrix, camera.getViewMatrix(), ProjectionMatrix);
	}

	//----------------------------------------------------------------------------------------------------
	// UI ------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------
	void renderGui(void) {
		//gui last
		if (toggle_ui) {
			glm::vec3 campos = camera.getPosition();
			int w = viewport_width;
			int h = viewport_height;
			ui.clearText();
			ui.addText(20, h - 15, 15, 15, std::string("p - toggle animation ") + (toggle_animation ? "ON" : "OFF"));
			ui.addText(20, h - 30, 15, 15, std::string("~ - toggle light moving ") + (toggle_light_moving ? "ON" : "OFF") + (" 1/2/3/4 light angles"));

			ui.addText(20, h - 45, 15, 15, std::string("g - toggle gui ") + (toggle_ui ? "ON" : "OFF"));
			ui.addText(20, h - 60, 15, 15, std::string("y - toggle vsync ") + +(toggle_vsync ? "ON" : "OFF"));
			ui.addText(20, h - 75, 15, 15, std::string("wasdrf+qe , RMB - movement , -+ camera speed "));
			ui.addText(20, h - 90, 15, 15, std::string("space - reload shaders "));
			std::string filteringText;
			if (toggle_filter == 0) filteringText = " Bilinear";
			if (toggle_filter == 1) filteringText = " Trilinear";
			if (toggle_filter == 2) filteringText = " Trilinear Anisotropic";
			ui.addText(20, h - 105, 15, 15, std::string("t - toggle filtering : ") + filteringText);
			ui.addText(20, h - 120, 15, 15, std::string("v - toggle Wireframe ") + (toggle_wireframe ? "ON" : "OFF"));
			ui.addText(20, h - 135, 15, 15, std::string("c - toggle Alpha Culling ") + (toggle_alpha_culling ? "ON" : "OFF"));
			ui.addText(20, h - 150, 15, 15, std::string("x,z - toggle Tess Steps outer/inner : " + std::to_string(toggle_tess_steps_outer) + "/" + std::to_string(toggle_tess_steps_inner)));
			ui.addText(20, h - 165, 15, 15, std::string("fragments picked = " + std::to_string(picking_buffer.getFragmentCountSolid()) + " solid and " + std::to_string(picking_buffer.getFragmentCountTransparent()) + " transparent"));
			ui.addText(20, h - 180, 15, 15, std::string("memory used = " + std::to_string(picking_buffer.getMemoryUsed())));
			ui.addText(20, h - 195, 15, 15, std::string("picked(solid): "));
			ui.addText(20, h - 210, 15, 15, std::string("   ObjectID    = ") + std::to_string(picking_buffer.fragments[0].objectId));
			ui.addText(20, h - 225, 15, 15, std::string("   PrimitiveID = ") + std::to_string(picking_buffer.fragments[0].primitiveId));
			ui.addText(20, h - 240, 15, 15, std::string("   VertexID    = ") + std::to_string(picking_buffer.fragments[0].vertexId));
			ui.addText(20, h - 255, 15, 15, std::string("   InstanceID  = ") + std::to_string(picking_buffer.fragments[0].instanceId));
			ui.addText(20, h - 270, 15, 15, std::string("   TessID      = ") + std::to_string((int)picking_buffer.fragments[0].invocationId));
			ui.addText(20, h - 285, 15, 15, std::string("   LightPercent= ") + std::to_string(picking_buffer.fragments[0].lightPercent));
			ui.addText(20, h - 300, 15, 15, std::string("   Alpha       = ") + std::to_string(picking_buffer.fragments[0].alpha));
			ui.addText(20, h - 315, 15, 15, std::string("   Depth       = ") + std::to_string(picking_buffer.fragments[0].depth));
			ui.addText(20, h - 330, 15, 15, std::string("picked(transparent): "));
			ui.addText(20, h - 345, 15, 15, std::string("   ObjectID    = ") + std::to_string(picking_buffer.fragments[1].objectId));
			ui.addText(20, h - 360, 15, 15, std::string("   PrimitiveID = ") + std::to_string(picking_buffer.fragments[1].primitiveId));
			ui.addText(20, h - 375, 15, 15, std::string("   VertexID    = ") + std::to_string(picking_buffer.fragments[1].vertexId));
			ui.addText(20, h - 390, 15, 15, std::string("   InstanceID  = ") + std::to_string(picking_buffer.fragments[1].instanceId));
			ui.addText(20, h - 405, 15, 15, std::string("   TessID      = ") + std::to_string((int)picking_buffer.fragments[1].invocationId));
			ui.addText(20, h - 420, 15, 15, std::string("   LightPercent= ") + std::to_string(picking_buffer.fragments[1].lightPercent));
			ui.addText(20, h - 435, 15, 15, std::string("   Alpha       = ") + std::to_string(picking_buffer.fragments[1].alpha));
			ui.addText(20, h - 450, 15, 15, std::string("   Depth       = ") + std::to_string(picking_buffer.fragments[1].depth));

			ui.addText(20, 30, 15, 15, std::string("Camera = ") + std::to_string(campos.x) + std::string(" , ") + std::to_string(campos.y) + std::string(" , ") + std::to_string(campos.z));
			ui.addText(w - 200, h - 15, 15, 15, std::string("FPS = ") + ground.getFPS());
			ui.render();
		}
	}

	//----------------------------------------------------------------------------------------------------
	// RENDER --------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------
	void render() {
		glViewport(0, 0, viewport_width, viewport_height);
		glEnable(GL_DEPTH_TEST);
		glClearColor(0.85f, 0.95f, 0.95f, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//simulate
		if (toggle_light_moving) {
			float u = 0.001f;
			float xx = light_position.x;
			float zz = light_position.z;
			light_position.x = xx*cos(u) - zz*sin(u);
			light_position.z = xx*sin(u) + zz*cos(u);
		}

		//clear and basic render state
		if (!toggle_wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		else glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		//pick buffer
		picking_buffer.bindForReadWrite(3, 4);

		//render scene
		renderScene();
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		renderParticleSystems();

		//finish picking
		if (picking_now) {
			picking_now = false;
			picking_buffer.getData();
		}


		//draw ui
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		renderGui();
	}

	void resize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		viewport_width = wnd.getFramebufferProperties().width;
		viewport_height = wnd.getFramebufferProperties().height;
		if (viewport_height == 0) viewport_height = 1;
		float ratio = (1.0f * viewport_width) / viewport_height;
		ProjectionMatrix = glm::perspective(glm::radians(53.13f), ratio, 1.0f, 30000.0f);
		ui.reshape(viewport_width, viewport_height);
	}

	//----------------------------------------------------------------------------------------------------
	// INPUT ----------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------
	void keyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		static float modificator = 1;
		switch (key) {
		case lap::wic::Key::ESCAPE:
			wnd.close();
			break;
		case lap::wic::Key::SPACE:
			//reload shaders
			std::cout << "-----------------------------------------------------------------------------" << std::endl;
			std::cout << "----------------------------------RELOAD-------------------------------------" << std::endl;
			std::cout << "-----------------------------------------------------------------------------" << std::endl;
			ui.reload();
			shader.Reload();
			particle_system.reloadShaders();
			break;
		case lap::wic::Key::P:
			toggle_animation = !toggle_animation;
		break;
		case lap::wic::Key::W:
			camera.translateForward(30 * modificator);	
			break;
		case lap::wic::Key::S:
			camera.translateForward(-30 * modificator);
			break;
		case lap::wic::Key::A: 
			camera.translateRight(-30 * modificator);
			break;
		case lap::wic::Key::D: 
			camera.translateRight(30 * modificator);
			break;
		case lap::wic::Key::R: 
			camera.translateUpword(30 * modificator);
			break;
		case lap::wic::Key::F:
			camera.translateUpword(-30 * modificator);
			break;
		case lap::wic::Key::Q: 
			camera.rotateY(0.1f*modificator);
			break;
		case lap::wic::Key::E: 
			camera.rotateY(-0.1f*modificator);
			break;
		case lap::wic::Key::MINUS: 
			modificator *= 0.95f; 						
			break;
		case lap::wic::Key::EQUAL: 
			modificator *= 1.05f;						
			break;
		case lap::wic::Key::Y:
			toggle_vsync = !toggle_vsync;
			if (toggle_vsync) wnd.setSwapInterval(-1);	//adaptive
			else wnd.setSwapInterval(0);				//none
			break;
		case lap::wic::Key::G:
			toggle_ui = !toggle_ui;
			break;
		case lap::wic::Key::T:
			toggle_filter = (toggle_filter + 1) % 3;
			if (toggle_filter == 0) ground.setFilteringBilinear();
			else if (toggle_filter == 1) ground.setFilteringTrilinear();
			else if (toggle_filter == 2) ground.setFilteringTrilinearAnisotropic();
			break;
		case lap::wic::Key::APOSTROPHE:				//pause resume light movement
			toggle_light_moving = !toggle_light_moving;
			break;
		case lap::wic::Key::NUM1:		//reinit
			light_position = glm::vec3(2800, 2500, 0);
			break;
		case lap::wic::Key::NUM2:		//view 2
			light_position = glm::vec3(0, 2500, 2800);
			break;
		case lap::wic::Key::NUM3:		//view 3
			light_position = glm::vec3(-2800, 2500, 0);
			break;
		case lap::wic::Key::NUM4:		//view 3
			light_position = glm::vec3(0, 2500, -2800);
			break;
		case lap::wic::Key::V:
			toggle_wireframe = !toggle_wireframe;
			break;
		case lap::wic::Key::C:
			toggle_alpha_culling = !toggle_alpha_culling;
			break;
		case lap::wic::Key::Z:
			if (toggle_tess_steps_outer == 64) toggle_tess_steps_outer = 1;
			else toggle_tess_steps_outer *= 2;
			break;
		case lap::wic::Key::X:
			if (toggle_tess_steps_inner == 64) toggle_tess_steps_inner = 1;
			else toggle_tess_steps_inner *= 2;
			break;
		default: break;
		}
	}
	void keyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void keyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		keyPress(wnd, key, alt, control, shift, system, state, timestamp);
	}
	void mousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		if (button == lap::wic::MouseButton::LEFT) {
			picking_now = true;
			picking_x = (float)posx;
			picking_y = wnd.getFramebufferProperties().height - (float)posy;
		}
	}
	void mouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void mouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		if (button == lap::wic::MouseButton::RIGHT) {
			static int oldposx = posx;
			static int oldposy = posy;
			float dx = glm::clamp((float)oldposx - posx, -25.f, 25.f);	//drag is reverse to movement
			float dy = glm::clamp((float)oldposy - posy, -25.f, 25.f);
			oldposx = posx;
			oldposy = posy;
			if (abs(dx) < 2 && abs(dy) < 2) return;
			camera.rotateY(dx / 400.0f);
			camera.rotateX(dy / 400.0f);
		}
	}
	void mouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void mouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}

private:
	//scene
	unsigned int viewport_width = 800, viewport_height = 600;
	rgp::Shader shader;
	rgp::Scene ground;
	rgp::Camera camera;
	rgp::UI ui;
	rgp::InstancedDrawer tree;
	glm::mat4 ModelMatrix, ProjectionMatrix;

	//toggles
	bool toggle_alpha_culling = true;
	bool toggle_animation = true;
	bool toggle_light_moving = false;
	bool toggle_vsync = true;
	bool toggle_ui = true;
	bool toggle_wireframe = false;
	int toggle_tess_steps_inner = 4;		//1 2 4 8 16 32 64
	int toggle_tess_steps_outer = 4;		//1 2 4 8 16 32 64
	int toggle_filter = 2;
	glm::vec3 light_position;

	//particles
	rgp::ParticleSystem particle_system;

	//picking
	bool picking_now = false;
	bool picking_show = true;
	float picking_x = 0;
	float picking_y = 0;
	rgp::PickingBuffer picking_buffer;
};








int main(int argc, char **argv) {
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "Rasterized GPU Picking";
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 4;	//4 spp
	cp.swap_interval = -1;		//adaptive vsync
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//app object
	App app(window);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&App::resize, &app, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&App::keyPress, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&App::keyRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&App::keyRepeat, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&App::mousePress, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&App::mouseRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&App::mouseDrag, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&App::mouseMove, &app, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&App::mouseScroll, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));

	//main loop
	while (window.isOpened()) {
		app.render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};


	return 0;
}