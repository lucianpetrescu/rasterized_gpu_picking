#include "ui.h"

namespace rgp {
	UI::UI() {
		screenwidth = screenheight = 0;
	}
	UI::~UI() {
		if (uivao != 0) glDeleteVertexArrays(1, &uivao);
		if (uivertices != 0) glDeleteBuffers(1, &uivertices);
		if (uitexcoords != 0) glDeleteBuffers(1, &uitexcoords);
		if (uitexture != 0) glDeleteTextures(1, &uitexture);
		if (uivao != 0) glDeleteVertexArrays(1, &uivao);
		if (uisampler != 0) glDeleteSamplers(1, &uisampler);
	}
	void UI::init(const std::string& fontfile) {
		if (uivao != 0) glDeleteVertexArrays(1, &uivao);
		glGenVertexArrays(1, &uivao);
		glBindVertexArray(uivao);
		if (uivertices != 0) glDeleteBuffers(1, &uivertices);
		glGenBuffers(1, &uivertices);
		if (uitexcoords != 0) glDeleteBuffers(1, &uitexcoords);
		glGenBuffers(1, &uitexcoords);
		if (uitexture != 0) glDeleteTextures(1, &uitexture);
		glGenTextures(1, &uitexture);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		FILE *file;	//256 *256 for %16 & /16..
		fopen_s(&file, fontfile.c_str(), "rb");
		int w, h; fread(&w, sizeof(int), 1, file); fread(&h, sizeof(int), 1, file);
		unsigned char *data = (unsigned char*)malloc(sizeof(unsigned char)*w*h * 3);
		fread(data, 3 * sizeof(unsigned char)*w*h, 1, file);
		glBindTexture(GL_TEXTURE_2D, uitexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		free(data);
		if (uisampler != 0) glDeleteSamplers(1, &uisampler);
		glGenSamplers(1, &uisampler);
		glSamplerParameteri(uisampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(uisampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(uisampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(uisampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		shader.Set(std::string("../shaders/ui.vert"), std::string("../shaders/ui.frag"));
		shader.Bind();

		vertices.reserve(1024);
		texcoords.reserve(1024);
	}
	void UI::reshape(int w, int h) {
		screenwidth = (float)w; screenheight = (float)h;
	}
	void UI::clearText() {
		texts.clear();
		vertices.clear();
		texcoords.clear();
	}
	void UI::addText(int startposx, int startposy, int wperchar, int hperchar, const std::string& text) {
		class UIText t;
		t.startposx = startposx;
		t.startposy = startposy;
		t.w = wperchar;
		t.h = hperchar;
		t.text = text;
		texts.push_back(t);
	}
	void UI::render() {	//variatie per frame => no baking
		vertices.clear(); texcoords.clear();
		int tricount = 0;
		float x[4], y[4], tx[4], ty[4], uvx, uvy;
		for (unsigned int i = 0; i < texts.size(); i++) {
			UIText t = texts[i];
			for (unsigned int k = 0; k < t.text.size(); k++) {
				//uv
				char ch = t.text[k];
				uvx = (ch % 16) / 16.0f;					uvy = (ch / 16) / 16.0f - 2.0f / 16;

				x[0] = (float)t.startposx + k*t.w;		tx[0] = uvx;						//v1 (down left)
				y[0] = (float)t.startposy;				ty[0] = 1 - (uvy + 1.0f / 16.0f);

				x[1] = (float)t.startposx + (k + 1)*t.w;	tx[1] = uvx + 1.0f / 16.0f;			//v2 (down right)
				y[1] = (float)t.startposy; 				ty[1] = 1 - (uvy + 1.0f / 16.0f);

				x[2] = (float)t.startposx + (k + 1)*t.w;	tx[2] = uvx + 1.0f / 16.0f;			//v3 (up right)
				y[2] = (float)t.startposy + t.h;			ty[2] = 1 - uvy;

				x[3] = (float)t.startposx + k*t.w;		tx[3] = uvx;						//v4 (up left)
				y[3] = (float)t.startposy + t.h;			ty[3] = 1 - uvy;

				vertices.push_back(x[0]);	vertices.push_back(y[0]);
				vertices.push_back(x[1]);	vertices.push_back(y[1]);
				vertices.push_back(x[2]);	vertices.push_back(y[2]);
				texcoords.push_back(tx[0]);	texcoords.push_back(ty[0]);
				texcoords.push_back(tx[1]);	texcoords.push_back(ty[1]);
				texcoords.push_back(tx[2]);	texcoords.push_back(ty[2]);

				vertices.push_back(x[2]);	vertices.push_back(y[2]);
				vertices.push_back(x[3]);	vertices.push_back(y[3]);
				vertices.push_back(x[0]);	vertices.push_back(y[0]);
				texcoords.push_back(tx[2]);	texcoords.push_back(ty[2]);
				texcoords.push_back(tx[3]);	texcoords.push_back(ty[3]);
				texcoords.push_back(tx[0]);	texcoords.push_back(ty[0]);
				tricount += 2;
			}
		}

		shader.Bind();
		shader.SetUniform("screen", screenwidth, screenheight);

		//bind texture
		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, uisampler);
		glBindTexture(GL_TEXTURE_2D, uitexture);
		shader.SetUniform("uiTexture", (int)0);

		//create
		glBindVertexArray(uivao);
		glBindBuffer(GL_ARRAY_BUFFER, uivertices);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, 0, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, uitexcoords);
		glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, 0);

		glDisable(GL_DEPTH_TEST);
		glDrawArrays(GL_TRIANGLES, 0, tricount * 3);
		glEnable(GL_DEPTH_TEST);
	}
	void UI::reload() {
		shader.Reload();
	}
}