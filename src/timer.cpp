#include "timer.h"

namespace rgp {
	Timer::Timer()
	{
		Reset();
	}
	void Timer::Reset()
	{
		mStart = std::chrono::high_resolution_clock::now();
	}
	int64_t Timer::Elapsed()
	{
		mCurrent = std::chrono::high_resolution_clock::now();
		return std::chrono::duration_cast<std::chrono::microseconds>(mCurrent - mStart).count();
	}
	int64_t Timer::ElapsedSinceLastCall()
	{
		auto rightNow = std::chrono::high_resolution_clock::now();
		auto result = rightNow - mCurrent;
		mCurrent = rightNow;
		return std::chrono::duration_cast<std::chrono::microseconds>(result).count();
	}
	float Timer::ElapsedSeconds() { 
		return Elapsed() / 1000000.0f; 
	}
	float Timer::ElapsedMilliSeconds() { 
		return Elapsed() / 1000.0f;
	}
	float Timer::ElapseMicroSeconds() {
		return Elapsed() / 1.0f; 
	}
	float Timer::ElapsedSinceLastCallSeconds() { 
		return ElapsedSinceLastCall() / 1000000.0f; 
	}
	float Timer::ElapsedSinceLastCallMilliSeconds() {
		return ElapsedSinceLastCall() / 1000.0f; 
	}
	float Timer::ElapsedSinceLastCallMicroSeconds() {
		return ElapsedSinceLastCall() / 1.0f; 
	}
}