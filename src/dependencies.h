
#pragma once
//OpenGL context + function pointers + input
#include "../dependencies/lap_wic/lap_wic.hpp"
//math
#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/type_ptr.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/quaternion.hpp"
