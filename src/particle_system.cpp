#include "particle_system.h"

namespace rgp {
	std::string strpass1VS = "../shaders/Pass1.vert";
	std::string strpass2VS = "../shaders/Pass2.vert";
	std::string strpass2GS = "../shaders/Pass2.geom";
	std::string strpass2FS = "../shaders/Pass2.frag";
	std::string strpass2TEX1 = "../assets/smoke2.img";
	std::string strpass2TEX2 = "../assets/smoke.img";


	ParticlePerturbator::ParticlePerturbator(float px, float py, float pz, float range, float power, float vx, float vy, float vz) {
		this->px = px;	this->py = py;	this->pz = pz;	this->range = range;
		this->vx = vx;	this->vy = vy;	this->vz = vz;	this->power = power;;
	}


		ParticleSystem::ParticleSystem() {
			srand((unsigned int)time(NULL)); 
		}
		ParticleSystem::~ParticleSystem() {
			if (idtextura1 != 0) glDeleteTextures(1, &idtextura1);
			if (idtextura2 != 0) glDeleteTextures(1, &idtextura2);
			if (idsampler != 0) glDeleteSamplers(1, &idsampler);
			if (vao[0] != 0 && vao[1] != 0) glDeleteVertexArrays(2, vao);
			if (vbostatic != 0) glDeleteBuffers(1, &vbostatic);
			if (vbo[0] != 0 && vbo[1] != 0) glDeleteBuffers(2, vbo);
			if (ubo_buffer != 0) glDeleteBuffers(1, &ubo_buffer);
		}

		void ParticleSystem::create(int count, float timetolive, float timetostart, float timetolivefade, float size) {
			//create shaders
			shaderPass1.Set(strpass1VS, true);
			shaderPass2.Set(strpass2VS, strpass2GS, strpass2FS);

			//textures
			FILE *file;
			fopen_s(&file, strpass2TEX1.c_str(), "rb");
			int w, h; fread(&w, sizeof(int), 1, file); fread(&h, sizeof(int), 1, file);
			unsigned char *data = (unsigned char*)malloc(sizeof(unsigned char)*w*h * 3);
			fread(data, 3 * sizeof(unsigned char)*w*h, 1, file);
			if (idtextura1 != 0) glDeleteTextures(1, &idtextura1);
			glGenTextures(1, &idtextura1);
			glBindTexture(GL_TEXTURE_2D, idtextura1);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			free(data); fclose(file);

			fopen_s(&file, strpass2TEX2.c_str(), "rb");
			fread(&w, sizeof(int), 1, file); fread(&h, sizeof(int), 1, file);
			data = (unsigned char*)malloc(sizeof(unsigned char)*w*h * 3);
			fread(data, 3 * sizeof(unsigned char)*w*h, 1, file);
			if (idtextura2 != 0) glDeleteTextures(1, &idtextura2);
			glGenTextures(1, &idtextura2);
			glBindTexture(GL_TEXTURE_2D, idtextura2);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			free(data);

			if (idsampler != 0) glDeleteSamplers(1, &idsampler);
			glGenSamplers(1, &idsampler);
			glSamplerParameteri(idsampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glSamplerParameteri(idsampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glSamplerParameteri(idsampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glSamplerParameteri(idsampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//create particule
			this->timetolivefade = timetolivefade;
			this->timetostart = timetostart;
			this->timetolive = timetolive;
			particlesDynamic.clear();
			particlesStatic.clear();
			particlesDynamic.reserve(count);
			particlesStatic.reserve(count);
			for (int i = 0; i < count; i++) {
				ParticleDynamic d;
				ParticleStatic s;
				d.px = s.sx = (rand() % 1000 - 500) / 8.f;
				d.py = s.sy = (rand() % 1000 - 500) / 8.f;
				d.pz = s.sz = (rand() % 1000 - 500) / 8.f;
				s.vx = 0.003f*(rand() % 100);
				s.vy = 0.01f*(rand() % 100);
				s.vz = 0.003f*(rand() % 100);
				float startbias = i / 3.0f*timetostart;
				s.oldttl = d.ttl = timetolive + (rand() % 400 - 200) / 1.f + startbias;
				s.size = size + (rand() % 100) / 25.f;
				particlesStatic.push_back(s);
				particlesDynamic.push_back(d);
			}

			//create 2 vaos
			if (vao[0] != 0 && vao[1] != 0) glDeleteVertexArrays(2, vao);
			glGenVertexArrays(2, vao);
			//2 buffers
			if (vbostatic != 0) glDeleteBuffers(1, &vbostatic);
			glGenBuffers(1, &vbostatic);
			if (vbo[0] != 0 && vbo[1] != 0) glDeleteBuffers(2, vbo);
			glGenBuffers(2, vbo);

			//fill buffers
			glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
			glBufferData(GL_ARRAY_BUFFER, count * sizeof(ParticleDynamic), &particlesDynamic[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
			glBufferData(GL_ARRAY_BUFFER, count * sizeof(ParticleDynamic), &particlesDynamic[0], GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, vbostatic);
			glBufferData(GL_ARRAY_BUFFER, count * sizeof(ParticleStatic), &particlesStatic[0], GL_STATIC_DRAW);

			//create rendering context 1
			glBindVertexArray(vao[0]);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);
			glEnableVertexAttribArray(3);
			glEnableVertexAttribArray(4);
			glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
			glVertexAttribPointer(0, 3, GL_FLOAT, 0, sizeof(ParticleDynamic), 0);
			glVertexAttribPointer(1, 1, GL_FLOAT, 0, sizeof(ParticleDynamic), (GLvoid*)(3 * sizeof(float)));
			glBindBuffer(GL_ARRAY_BUFFER, vbostatic);
			glVertexAttribPointer(2, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(0 * sizeof(float)));
			glVertexAttribPointer(3, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(3 * sizeof(float)));
			glVertexAttribPointer(4, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(6 * sizeof(float)));

			//create buffer 2
			glBindVertexArray(vao[1]);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);
			glEnableVertexAttribArray(3);
			glEnableVertexAttribArray(4);
			glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
			glVertexAttribPointer(0, 3, GL_FLOAT, 0, sizeof(ParticleDynamic), 0);
			glVertexAttribPointer(1, 1, GL_FLOAT, 0, sizeof(ParticleDynamic), (GLvoid*)(3 * sizeof(float)));
			glBindBuffer(GL_ARRAY_BUFFER, vbostatic);
			glVertexAttribPointer(2, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(0 * sizeof(float)));
			glVertexAttribPointer(3, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(3 * sizeof(float)));
			glVertexAttribPointer(4, 3, GL_FLOAT, 0, sizeof(ParticleStatic), (GLvoid*)(6 * sizeof(float)));

			//get varyings pentru transform feedback
			const char* outputs[] = { "out_position", "out_ttl" };
			glTransformFeedbackVaryings(shaderPass1.GetGLShader(), 2, outputs, GL_INTERLEAVED_ATTRIBS);
			glLinkProgram(shaderPass1.GetGLShader());

			ping = true;
		}

		void ParticleSystem::reloadShaders() {
			shaderPass1.Reload();
			shaderPass2.Reload();
			const char* outputs[] = { "out_position", "out_ttl" };
			glTransformFeedbackVaryings(shaderPass1.GetGLShader(), 2, outputs, GL_INTERLEAVED_ATTRIBS);
			glLinkProgram(shaderPass1.GetGLShader());
		}

		void ParticleSystem::computeCPU() {

			float dt = 0.1f;//constant
			for (unsigned int i = 0; i < particlesStatic.size(); i++) {
				if ((particlesStatic[i].oldttl - particlesDynamic[i].ttl) > timetostart) {
					particlesDynamic[i].px += particlesStatic[i].vx*dt;
					particlesDynamic[i].py += particlesStatic[i].vy*dt;
					particlesDynamic[i].pz += particlesStatic[i].vz*dt;


					for (unsigned int j = 0; j < perturbators.size(); j++) {
						float dx = particlesDynamic[i].px - perturbators[j].px;
						float dy = particlesDynamic[i].py - perturbators[j].py;
						float dz = particlesDynamic[i].pz - perturbators[j].pz;
						float dist = sqrt(dx*dx + dy*dy + dz*dz);
						if (dist < perturbators[j].range) {
							particlesDynamic[i].px += perturbators[j].vx*dt*perturbators[j].power;
							particlesDynamic[i].py += perturbators[j].vy*dt*perturbators[j].power;
							particlesDynamic[i].pz += perturbators[j].vz*dt*perturbators[j].power;
						}
					}

				}
				particlesDynamic[i].ttl -= 1;
				if (particlesDynamic[i].ttl < 0) {
					particlesDynamic[i].px = particlesStatic[i].sx;
					particlesDynamic[i].py = particlesStatic[i].sy;
					particlesDynamic[i].pz = particlesStatic[i].sz;
					particlesDynamic[i].ttl = particlesStatic[i].oldttl;
				}
			}
			if (ping) {
				glBindVertexArray(vao[1]);
				glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
			}
			else {
				glBindVertexArray(vao[0]);
				glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
			}
			glBufferData(GL_ARRAY_BUFFER, particlesDynamic.size() * sizeof(ParticleDynamic), &particlesDynamic[0], GL_DYNAMIC_DRAW);
		}

		void ParticleSystem::computeGPU() {
			glUseProgram(shaderPass1.GetGLShader());
			int locdt = glGetUniformLocation(shaderPass1.GetGLShader(), "dt");
			float dt = 0.1f;
			glUniform1f(locdt, dt);
			int loctimetostart = glGetUniformLocation(shaderPass1.GetGLShader(), "timetostart");
			glUniform1f(loctimetostart, timetostart);

			GLuint query;
			glGenQueries(1, &query);
			glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
			glEnable(GL_RASTERIZER_DISCARD);

			if (ping) {			//0->1
				glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[1]);
				glBeginTransformFeedback(GL_POINTS);
				glBindVertexArray(vao[0]);
				glDrawArrays(GL_POINTS, 0, (int)particlesDynamic.size());
			}
			else {				//1->0
				glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[0]);
				glBeginTransformFeedback(GL_POINTS);
				glBindVertexArray(vao[1]);
				glDrawArrays(GL_POINTS, 0, (int)particlesDynamic.size());
			}

			glEndTransformFeedback();
			glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
			glDisable(GL_RASTERIZER_DISCARD);
			glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
			GLuint primitiveswritten;
			glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitiveswritten);

			//flip
			ping = !ping;
		}

		void ParticleSystem::simulate(bool withgpu) {
			if (!withgpu) computeCPU();
			else computeGPU();
		}

		void ParticleSystem::render(const glm::mat4& ModelMatrix, const glm::mat4& ViewMatrix, const glm::mat4& ProjMatrix) {
			//pass 2
			glDepthMask(GL_FALSE);
			glEnable(GL_BLEND);
			glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
			glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
			glUseProgram(shaderPass2.GetGLShader());
			int ProjectionMatrixLocation = glGetUniformLocation(shaderPass2.GetGLShader(), "projMatrix");
			int ViewMatrixLocation = glGetUniformLocation(shaderPass2.GetGLShader(), "viewMatrix");
			int ModelMatrixLocation = glGetUniformLocation(shaderPass2.GetGLShader(), "modelMatrix");
			glUniformMatrix4fv(ProjectionMatrixLocation, 1, false, glm::value_ptr(ProjMatrix));
			glUniformMatrix4fv(ViewMatrixLocation, 1, false, glm::value_ptr(ViewMatrix));
			glUniformMatrix4fv(ModelMatrixLocation, 1, false, glm::value_ptr(ModelMatrix));
			int ttlfade = glGetUniformLocation(shaderPass2.GetGLShader(), "ttlfade");
			glUniform1f(ttlfade, timetolivefade);
			int ttlstart = glGetUniformLocation(shaderPass2.GetGLShader(), "ttlstart");
			glUniform1f(ttlstart, timetostart);
			glActiveTexture(GL_TEXTURE0);
			int texloc1 = glGetUniformLocation(shaderPass2.GetGLShader(), "texMap1");
			glBindTexture(GL_TEXTURE_2D, idtextura1);
			glUniform1i(texloc1, 0);
			glActiveTexture(GL_TEXTURE1);
			int texloc2 = glGetUniformLocation(shaderPass2.GetGLShader(), "texMap2");
			glBindTexture(GL_TEXTURE_2D, idtextura2);
			glUniform1i(texloc2, 1);
			glBindSampler(0, idsampler);
			glBindSampler(1, idsampler);

			if (ping) glBindVertexArray(vao[1]);		//ping: 0->1
			else glBindVertexArray(vao[0]);				//pong: 1->0

			glDrawArrays(GL_POINTS, 0, (int)particlesDynamic.size());
			glDisable(GL_BLEND);
			glDepthMask(GL_TRUE);
		}

		void ParticleSystem::addPerturbator(const ParticlePerturbator& a) {
			perturbators.push_back(a);

			//create memory
			float *buff = (float*)malloc(sizeof(ParticlePerturbator)*perturbators.size());
			for (unsigned int i = 0; i < perturbators.size(); i++) {
				buff[i * 4 + 0] = perturbators[i].px;
				buff[i * 4 + 1] = perturbators[i].py;
				buff[i * 4 + 2] = perturbators[i].pz;
				buff[i * 4 + 3] = perturbators[i].range;
				buff[i * 4 + 0 + 4 * perturbators.size()] = perturbators[i].vx;
				buff[i * 4 + 1 + 4 * perturbators.size()] = perturbators[i].vy;
				buff[i * 4 + 2 + 4 * perturbators.size()] = perturbators[i].vz;
				buff[i * 4 + 3 + 4 * perturbators.size()] = perturbators[i].power;
			}
			//create ubo
			glDeleteBuffers(1, &ubo_buffer);
			glGenBuffers(1, &ubo_buffer);
			glBindBuffer(GL_UNIFORM_BUFFER, ubo_buffer);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(ParticlePerturbator)*perturbators.size(), buff, GL_STATIC_DRAW);
			free(buff);

			//ubo
			ubo_binding_point = 0;
			glBindBufferBase(GL_UNIFORM_BUFFER, ubo_binding_point, ubo_buffer);
			ubo_block_index = glGetUniformBlockIndex(shaderPass1.GetGLShader(), "Perturbators");
			glUniformBlockBinding(shaderPass1.GetGLShader(), ubo_block_index, ubo_binding_point);

			// don't ever do this, this is JUST an academic example
			mangleShader();
		}
		void ParticleSystem::clearPerturbators() {
			perturbators.clear();
		}
		void ParticleSystem::littletokenizer(const std::string& str, std::vector<std::string> *toks) {
			std::stringstream ss;
			ss << str;
			std::string s;
			toks->clear();
			while (ss.good()) {
				ss >> s; toks->push_back(s);
			}
		}
		//JUST for academic examples, otherwise a TOOL should be doing this
		void ParticleSystem::mangleShader() {
			std::ifstream file(strpass1VS.c_str());
			std::string line;
			std::vector<std::string> toks;
			std::string shadercode = "";
			if (file.is_open()) {
				while (file.good()) {
					getline(file, line);
					littletokenizer(line, &toks);
					if (toks.size() == 3) {
						if (toks[0] == "#define" && toks[1] == "COUNT") {
							toks[2] = std::to_string(perturbators.size());
						}
					}
					shadercode += line + "\n";
				}
				file.close();
			}

			//modify shader file
			std::ofstream outfile(strpass1VS.c_str(), std::ios::out);
			outfile << shadercode;
			outfile.close();
			shaderPass1.Set(strpass1VS, true);

			//rebuild transform feedback varying bindings
			const char* outputs[] = { "out_position", "out_ttl" };
			glTransformFeedbackVaryings(shaderPass1.GetGLShader(), 2, outputs, GL_INTERLEAVED_ATTRIBS);
			glLinkProgram(shaderPass1.GetGLShader());
		}
		Shader& ParticleSystem::getProgramRender() { 
			return shaderPass2; 
		}
}